#include "wpr-ntable.h"
#include "ns3/simulator.h"
#include <iomanip>
#include "ns3/log.h"

NS_LOG_COMPONENT_DEFINE ("WprNeighborTable");

namespace ns3 {
namespace wpr {
std::map<uint64_t, std::vector<Ipv4Address> > cache;
Neighbor::Neighbor (Vector pos, Ipv4Address addr)
  : m_pos (pos),
    m_addr (addr)
{
	m_hops = 0;
	m_dist = 0;
}
Neighbor::~Neighbor ()
{
}

void
Neighbor::IncreaseHops(int inc){
	m_hops += inc;
}
NeighborTable::NeighborTable (Vector pos)
  : m_pos (pos)
{
}

NeighborTable::~NeighborTable ()
{
}

bool
NeighborTable::LookupNeighbor (Ipv4Address id,
                           Neighbor & n)
{
  if (m_ipv4AddressEntry.empty ())
    {
      return false;
    }
  std::map<Ipv4Address, Neighbor>::const_iterator i = m_ipv4AddressEntry.find (id);
  if (i == m_ipv4AddressEntry.end ())
    {
      return false;
    }
  n = i->second;
  return true;
}

bool
NeighborTable::LookupNeighbor (int id,
                           Neighbor & n)
{
	
	std::map<Ipv4Address, Neighbor>::const_iterator i = m_ipv4AddressEntry.begin();
	std::advance (i,id);
  	if (i == m_ipv4AddressEntry.end ()){
	      return false;
	}
	n = i->second;
	return true;
}
bool
NeighborTable::LookupNeighbor (Ipv4Address id){
  if (m_ipv4AddressEntry.empty ()){
      return false;
    }
  std::map<Ipv4Address, Neighbor>::const_iterator i = m_ipv4AddressEntry.find (id);
  if (i == m_ipv4AddressEntry.end ())
    {
      return false;
    }
  return true;
}
bool
NeighborTable::LookupClosestNeighbor (Vector pos, Neighbor & n, Packet packet, Neighbor previousHop)
{
	
	std::map<Ipv4Address, Neighbor>::iterator j;
	double min_dist = -1;
	for (j = m_ipv4AddressEntry.begin (); j != m_ipv4AddressEntry.end (); ++j){
		if(PacketInCache(packet, j->second.GetAddress())){
			NS_LOG_DEBUG("Already sent to this neighbor - skipping");
			 continue;
		}
		if(j->second.GetAddress() == previousHop.GetAddress()){
			NS_LOG_DEBUG("My previous hop - skipping");
			continue;
		}
		//std::cout <<"Calculating distance my_pos:" << m_pos << " previous:" << previousHop.GetPosition() << " chosen:" << j->second.GetPosition() << "\n";
		if(CalculateDistance(m_pos, previousHop.GetPosition()) > CalculateDistance(j->second.GetPosition(), previousHop.GetPosition())){
			NS_LOG_DEBUG("New hop is closer to previous hop - skipping");
		//	std::cout << "New hop is closer to previous hop - skipping \n";
			continue;
		}
		double cur_dist = CalculateDistance (pos, j->second.GetPosition());
		NS_LOG_DEBUG( "dist: " << cur_dist <<"\n");
		if((min_dist < 0) || (cur_dist < min_dist))
		{
			n = j->second;
			min_dist = cur_dist;
		}
	}
	if(min_dist < 0)
	{
		return false;
	}else{
		return true;
	}

}


bool
NeighborTable::DeleteNeighbor (Ipv4Address n)
{
  if (m_ipv4AddressEntry.erase (n) != 0)
    {
      return true;
      UpdateSpaces();
    }
  return false;
}

bool
NeighborTable::AddNeighbor (Neighbor & n)
{
  std::pair<std::map<Ipv4Address, Neighbor>::iterator, bool> result = m_ipv4AddressEntry.insert (std::make_pair (
                                                                                                            n.GetAddress (),n));
  UpdateSpaces();
  return result.second;
}

uint32_t
NeighborTable::NeighborTableSize ()
{
  return m_ipv4AddressEntry.size ();
}

void
NeighborTable::Print (Ptr<OutputStreamWrapper> stream) const
{
  return;
  *stream->GetStream () << "\nWPR Neighbor table\n" << "Addr\t\tPosition\n";
  for (std::map<Ipv4Address, Neighbor>::const_iterator i = m_ipv4AddressEntry.begin (); i
       != m_ipv4AddressEntry.end (); ++i)
    {
      i->second.Print (stream);
    }
  *stream->GetStream () << "\n";
}


void
NeighborTable::Print (std::ostream &os) const
{
  return;
  os << "\nWPR Neighbor table\n" << "Addr\t\tPosition\n";
  for (std::map<Ipv4Address, Neighbor>::const_iterator i = m_ipv4AddressEntry.begin (); i
       != m_ipv4AddressEntry.end (); ++i)
    {
      os << i->second;
    }
  os << "\n";
}


void
NeighborTable::CalculateDiameter ()
{
  int max_dist = 0;

  for (std::map<Ipv4Address, Neighbor>::const_iterator i = m_ipv4AddressEntry.begin (); i
       != m_ipv4AddressEntry.end (); ++i)
    {
      double cur_dist = CalculateDistance (m_pos, i->second.GetPosition());
      if(cur_dist > max_dist)
	max_dist = cur_dist;
    }
  NS_LOG_DEBUG("Diameter set to: " << 2 * max_dist << "\n"); 
  m_diameter = 2 * max_dist;
}

#if D3
void 
NeighborTable::UpdateSpaces(){
        unsigned int tx = ax;
        unsigned int ty = ay;
        unsigned int tz = az;

        unsigned int x = 0;
        unsigned int y = 0;
        unsigned int z = 0;

        int i = 0;
        CalculateDiameter();
        //printf("update spaces (d:%d): ", diameter);
        while((tx > m_diameter) && (ty > m_diameter) && (tz > m_diameter) && (i < MAX_SPACES_DEPTH * SPACES_DIVISION)){
                tx /= 2;
                ty /= 2;
                tz /= 2;
                //printf("tx:%d, ty:%d self(%d.%d)\n", tx, ty, self.pos.x, self.pos.y);
                if(m_pos.x < (x + tx)){

                        if(m_pos.y < (y + ty)){
                                if(m_pos.z < (z + tz)){
                                        m_spaces[i] = 0;
                                }else{
                                        m_spaces[i] = 4;
                                        z += tz;
                                }
                        }else{
                                if(m_pos.z < (z + tz)){
                                        m_spaces[i] = 1;
                                }else{
                                        m_spaces[i] = 5;
                                        z += tz;
                                }
                                y += ty;
                        }
                }else{
                        x += tx;
                        if(m_pos.y < (y + ty)){
                                if(m_pos.z < (z + tz)){
                                        m_spaces[i] = 2;
                                }else{
                                        m_spaces[i] = 6;
                                        z += tz;
                                }
                        }else{
                                if(m_pos.z < (z + tz)){
                                         m_spaces[i] = 3;
                                }else{
                                        m_spaces[i] = 7;
                                        z += tz;
                                }
                                y += ty;
                        }
                }
                //printf(" %d", spaces[i]);
                i++;

        }
        if(i < MAX_SPACES_DEPTH * SPACES_DIVISION)
                m_spaces[i] = -1;
}


#else
void 
NeighborTable::UpdateSpaces()
{
	unsigned int tx = ax;
        unsigned int ty = ay;

        unsigned int x = 0;
        unsigned int y = 0;

        int i = 0;
        CalculateDiameter();
        //printf("update spaces (d:%d): ", diameter);
        while((tx > m_diameter) && (ty > m_diameter) && (i < MAX_SPACES_DEPTH * SPACES_DIVISION)){
                tx /= 2;
                ty /= 2;
                //printf("tx:%d, ty:%d self(%d.%d)\n", tx, ty, self.pos.x, self.pos.y);
                if(m_pos.x < (x + tx)){

                        if(m_pos.y < (y + ty)){
                                m_spaces[i] = 0;
                        }else{
                                m_spaces[i] = 1;
                                y += ty;
                        }
                }else{
                        if(m_pos.y < (y + ty)){
                                m_spaces[i] = 3;
                        }else{
                                m_spaces[i] = 2;
                                y += ty;
                        }
                        x += tx;
                }
                //printf(" %d", spaces[i]);
                i++;

        }
        if(i < MAX_SPACES_DEPTH * SPACES_DIVISION)
                m_spaces[i] = -1;


}
#endif /* D3 */

bool
NeighborTable::CachePacket(Packet packet, Ipv4Address addr){

        std::map<uint64_t, std::vector<Ipv4Address> >::iterator mapIt;
        if( (mapIt = cache.find(packet.GetUid())) == cache.end()){
                std::vector<Ipv4Address> v;
                v.push_back(addr);
                cache.insert (std::make_pair (packet.GetUid(), v));
        }else{
                mapIt->second.push_back(addr);
        }

        while(cache.size() > MAX_CACHE){
                cache.erase(cache.begin());
        }
        return true;
}


bool
NeighborTable::PacketInCache(Packet packet, Ipv4Address addr){
        std::map<uint64_t, std::vector<Ipv4Address> >::iterator mapIt;
        if( (mapIt = cache.find(packet.GetUid())) == cache.end()){
                return false;
        }else{
                std::vector<Ipv4Address>::iterator vectorIt;
                for(vectorIt = mapIt->second.begin(); vectorIt != mapIt->second.end(); vectorIt++){
                        if(*vectorIt == addr)
                                return true;
                }
        }
        return false;
}




void
Neighbor::Print (Ptr<OutputStreamWrapper> stream) const
{
  *stream->GetStream () << std::setiosflags (std::ios::fixed) << m_addr << "\t\t" << m_pos << "\t\t" << m_hops << "\n";

}

void
Neighbor::Print (std::ostream &os) const
{
  os << m_addr << "\t\t" << m_pos << "\n";

}

}
}
