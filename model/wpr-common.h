#ifndef WPR_COMMON
#define WPR_COMMON
#define D3 0
#define MAX_TRACE 5 
#define MAX_ENTRY_POINTS 1 
#define MAX_WAYPOINTS 3 
#define WITH_BALANCED_WAYPOINTS 1 

#define MAX_SPACES_DEPTH 8
#if D3 
#define SPACES_DIVISION 8
#else
#define SPACES_DIVISION 4
#endif
#define MAX_CACHE 5


#endif /* WPR_COMMON */
