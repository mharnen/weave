#include "wpr-routing-protocol.h"
#include "ns3/log.h"
#include "ns3/inet-socket-address.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/udp-socket-factory.h"
#include "ns3/wifi-net-device.h"
#include "ns3/boolean.h"
#include "ns3/double.h"
#include "ns3/uinteger.h"
#include "ns3/udp-header.h"
#include "ns3/ipv4-header.h"
#include "ns3/icmpv4.h"

#include "wpr-common.h"
NS_LOG_COMPONENT_DEFINE ("WprRoutingProtocol");

double ax;
double ay;
#if D3
double az;
#endif

namespace ns3 {
namespace wpr {
NS_OBJECT_ENSURE_REGISTERED (RoutingProtocol);
/// UDP Port for WPR control traffic
const uint32_t RoutingProtocol::WPR_PORT = 269;
NeighborTable allNodesTable;
std::vector<uint64_t> droped;
std::map<int, Ipv4Address> off;
/// Tag used by WPR implementation

void RoutingProtocol::UpdateOff(){
	uint32_t size = (uint32_t) allNodesTable.NeighborTableSize() * m_lossRate * 0.01;
//	std::cout << "Calculated size: " << size << "\n";
	std::map<int, Ipv4Address >::iterator i;
	while(off.size() <= size){
		i = off.begin();// na poczatku end == begin
		int chosen = -1;
		while( (chosen == -1) || (i != off.end()) ){
			chosen = rand() % allNodesTable.NeighborTableSize();
			i = off.find (chosen);
		}
		Neighbor n;
		allNodesTable.LookupNeighbor(chosen, n);
		off.insert (std::make_pair (chosen, n.GetAddress ()));
	}
	i = off.begin();
	std::advance (i,rand() % off.size());
	off.erase(i);
	NS_LOG_DEBUG("Nodes switched off");
	for (std::map<int, Ipv4Address>::const_iterator i = off.begin (); i != off.end (); ++i)
	    {
	      NS_LOG_DEBUG(i->second);
	    }
}

	

bool RoutingProtocol::CheckOff(Ipv4Address addr){
	NS_LOG_INFO( "Checking for " << addr << ":");
	std::map<int, Ipv4Address>::const_iterator i = off.begin();
	for (std::map<int, Ipv4Address>::const_iterator i = off.begin (); i != off.end (); ++i){
	    	if(i->second == addr){
			NS_LOG_INFO("down");
			return true;
		}
        }
	NS_LOG_INFO("up");
	return false;
}

TypeId
RoutingProtocol::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::wpr::RoutingProtocol")
    .SetParent<Ipv4RoutingProtocol> ()
    .AddConstructor<RoutingProtocol> ()
    .AddAttribute ("PeriodicUpdateInterval","Periodic interval between exchange of full routing tables among nodes. ",
                   TimeValue (Seconds (15)),
                   MakeTimeAccessor (&RoutingProtocol::m_periodicUpdateInterval),
                   MakeTimeChecker ())
    .AddAttribute ("withWaypoint", "Shall we use waypoints.",
                   BooleanValue (true),
                   MakeBooleanAccessor (&RoutingProtocol::m_withWaypoint),
                   MakeBooleanChecker ())
    .AddAttribute ("withEntryPoint", "Shall we use entry points.",
                   BooleanValue (true),
                   MakeBooleanAccessor (&RoutingProtocol::m_withEntryPoint),
                   MakeBooleanChecker ())
    .AddAttribute ("withReversePacket", "Shall we use reverse packet.",
                   BooleanValue (true),
                   MakeBooleanAccessor (&RoutingProtocol::m_withReversePacket),
                   MakeBooleanChecker ())
    .AddAttribute ("withExplore", "Shall we use path exploring.",
                   BooleanValue (true),
                   MakeBooleanAccessor (&RoutingProtocol::m_withExplore),
                   MakeBooleanChecker ())
    .AddAttribute ("MaxX", "Maximum X dimension.",
                   DoubleValue (400.0),
                   MakeDoubleAccessor (&RoutingProtocol::m_maxX),
                   MakeDoubleChecker<double> ())
    .AddAttribute ("MaxY", "Maximum Y dimension.",
                   DoubleValue (400.0),
                   MakeDoubleAccessor (&RoutingProtocol::m_maxY),
                   MakeDoubleChecker<double> ())
#if D3
    .AddAttribute ("MaxZ", "Maximum Z dimension.",
                   DoubleValue (400.0),
                   MakeDoubleAccessor (&RoutingProtocol::m_maxZ),
                   MakeDoubleChecker<double> ())
#endif /* D3 */
    .AddAttribute ("lossRate", "Loss rate",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&RoutingProtocol::m_lossRate),
                   MakeDoubleChecker<double> ());
    /*.AddAttribute ("maxWaypoints", "Maximum number of waypoints par space",
                   UintegerValue (3),
                   MakeUintegerAccessor (&WaypointTable::m_maxWaypoints),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("withBalancedWaypoints", "Shall we use balanced waypoints.",
                   BooleanValue (true),
                   MakeBooleanAccessor (&WaypointTable::m_withBalancedWaypoints),
                   MakeBooleanChecker ())
    .AddAttribute ("maxTrace", "Maximum size of trace",
                   UintegerValue (3),
                   MakeUintegerAccessor (&Waypoint::m_maxTrace),
                   MakeUintegerChecker<uint32_t> ());*/


  return tid;
}
void
RoutingProtocol::SetPosition (Vector pos)
{
  m_position = pos;
  m_neighborTable.SetPosition(pos);
  ax = m_maxX;
  ay = m_maxY;
#if D3
  az = m_maxZ;
#endif
  //std::cout << "Set xy: " << ax << ay << "\n";
}
Vector
RoutingProtocol::GetPosition () const
{
  return m_position;
}

int64_t
RoutingProtocol::AssignStreams (int64_t stream)
{
  NS_LOG_FUNCTION (this << stream);
  m_uniformRandomVariable->SetStream (stream);
  return 1;
}

RoutingProtocol::RoutingProtocol ()
{
  m_uniformRandomVariable = CreateObject<UniformRandomVariable> ();
  m_counter = 10;
}

RoutingProtocol::~RoutingProtocol ()
{
}

void
RoutingProtocol::DoDispose ()
{
  m_ipv4 = 0;
  for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::iterator iter = m_socketAddresses.begin (); iter
       != m_socketAddresses.end (); iter++)
    {
      iter->first->Close ();
    }
  m_socketAddresses.clear ();
  Ipv4RoutingProtocol::DoDispose ();
}

void
RoutingProtocol::PrintRoutingTable (Ptr<OutputStreamWrapper> stream) const
{
  std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin ();
  Ipv4InterfaceAddress iface = j->second;
  Ipv4Address addr = iface.GetLocal ();

  *stream->GetStream () << "Node: " << addr << m_neighborTable << "\n";
	
 /* for(std::vector<uint64_t>::const_iterator i = droped.begin (); i != droped.end(); i++){
  	*stream->GetStream () << "Droped: " << *i << "\n";
  }*/
}

void
RoutingProtocol::Start ()
{
  m_scb = MakeCallback (&RoutingProtocol::Send,this);
  m_periodicUpdateTimer.SetFunction (&RoutingProtocol::SendPeriodicUpdate,this);
  m_periodicUpdateTimer.Schedule (MicroSeconds (m_uniformRandomVariable->GetInteger (0,9000)));


  std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin ();
  Ipv4InterfaceAddress iface = j->second;
  Ipv4Address addr = iface.GetLocal ();
  Neighbor n (m_position, addr);
  allNodesTable.AddNeighbor (n);
  Ptr<Ipv4L3Protocol> l3 = m_ipv4->GetObject<Ipv4L3Protocol> ();
  l3->SetDefaultTtl(250);  	
}

Ptr<Ipv4Route>
RoutingProtocol::RouteOutput (Ptr<Packet> p,
                              const Ipv4Header &header,
                              Ptr<NetDevice> oif,
                              Socket::SocketErrno &sockerr)
{

Ptr<Packet> packet = p->Copy ();
int prot = (int) header.GetProtocol();
        if(prot == 1){
                Icmpv4Header icmpHeader;
                NS_LOG_ERROR("Sending ICMP(" << header <<") - drop");
                packet->RemoveHeader (icmpHeader);

                NS_LOG_ERROR("Sending ICMP(" << icmpHeader <<" - drop");
               // NS_ASSERT(0);
        }


	
	Ipv4Route* route = new Ipv4Route ();
	NS_LOG_INFO("header: " << header << " device ptr: " << oif << " packet " << *p << "\n");


	
	std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin ();
	Ipv4InterfaceAddress iface = j->second;	
	Ipv4Address src = iface.GetLocal ();
	Ipv4Address dst = header.GetDestination();
	Ptr<NetDevice> dev = m_ipv4->GetNetDevice (m_ipv4->GetInterfaceForAddress (iface.GetLocal ()));

	route->SetDestination (dst);
	route->SetSource (src);
 	route->SetOutputDevice (dev);
	Ipv4Address gw;
 	if(dst.IsBroadcast() || dst == iface.GetBroadcast()){
		route->SetGateway (dst);
		NS_LOG_INFO(iface.GetLocal() << " is sending broadcast \n");
		return route;
	}else{
		// Add waypoint header 
		UpdateOff();
   		Waypoint srcWaypoint (m_position, src, 0);
		WprWaypointHeader wwHeader;
		wwHeader.SetReverse(false);
		wwHeader.SetSrc(srcWaypoint);
		

		Neighbor target;
		Neighbor neighbor;
		if(allNodesTable.LookupNeighbor (dst, target)){
			NS_LOG_INFO( "Translated " << dst << " to " << target.GetPosition() << "\n");
			if(!ChooseGateway(target, wwHeader, gw, *p)){
				NS_LOG_ERROR ("No route to host. Dropped." << p->GetUid() << " " << dst);
				NS_LOG_WARN (wwHeader);
				droped.push_back(p->GetUid());
				//NS_LOG_WARN (m_neighborTable);
		                sockerr = Socket::ERROR_NOROUTETOHOST;
  		                return Ptr<Ipv4Route> ();
			}else{
				/*if(p->GetUid() == 1114){
				NS_LOG_ERROR ("Special." << p->GetUid() << " " << dst);
				NS_LOG_WARN (m_neighborTable);
				NS_LOG_WARN (wwHeader);
				}*/
				p->AddHeader(wwHeader);
				route->SetGateway(gw);
				NS_LOG_DEBUG("caching packet: " << p->GetUid() << " with gw: " << gw);
				m_neighborTable.CachePacket(*p, gw);
			}
		}else{
			NS_LOG_ERROR("Couldn't find a node with IP: " << dst << "\n");
		        sockerr = Socket::ERROR_NOROUTETOHOST;
  		        return Ptr<Ipv4Route> ();
		}
		NS_LOG_DEBUG("Sending header: " << wwHeader <<"\n");
	}
	if(CheckOff(gw)){
	        NS_LOG_DEBUG("Host is down!\n");
		m_neighborTable.PurgeNode(route->GetGateway());
  		return Ptr<Ipv4Route> ();
	}

	int ttl = (int) header.GetTtl();
	NS_LOG_INFO("Constructed route: " << *route << "\n");
	NS_LOG_ERROR( "!" << p->GetUid () << " " << iface.GetLocal () << "(" << p->GetUid() << ")(" << ttl << ")->" << gw << "->" << dst);
//	NS_LOG_ERROR(m_neighborTable);
	return route; 
}


bool
RoutingProtocol::RouteInput (Ptr<const Packet> p,
                             const Ipv4Header &header,
                             Ptr<const NetDevice> idev,
                             UnicastForwardCallback ucb,
                             MulticastForwardCallback mcb,
                             LocalDeliverCallback lcb,
                             ErrorCallback ecb)
{

Ptr<Packet> packet = p->Copy ();
int prot = (int) header.GetProtocol();
NS_LOG_INFO (m_mainAddress << " received packet " << p->GetUid ()
                                 << " from " << header.GetSource ()
                                 << " on interface " << idev->GetAddress ()
                                 << " to destination " << header.GetDestination ()
				 << " protocol " << prot);
	if(prot == 1){
		Icmpv4Header icmpHeader;
		NS_LOG_ERROR("Got CMP(" << header <<" - drop");
		packet->RemoveHeader (icmpHeader);

		NS_LOG_ERROR("Got ICMP(" << icmpHeader <<" - drop");
//		NS_ASSERT(0);
		return false;
	}

	Ipv4Address dst = header.GetDestination ();
	Ipv4Address origin = header.GetSource ();
	std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin ();
	Ipv4InterfaceAddress iface = j->second;
	int32_t iif = m_ipv4->GetInterfaceForDevice (idev);

	if(dst == iface.GetBroadcast () || dst.IsBroadcast ())
        {
//                NS_LOG_ERROR("#Broadcast\n");

		if (lcb.IsNull () == false)
                {
                  NS_LOG_INFO ("Broadcast local delivery to " << iface.GetLocal ());
                  lcb (p, header, iif);
		}else{
		  NS_LOG_ERROR ("Unable to deliver packet locally due to null callback " << p->GetUid () << " from " << origin);
                  ecb (p, header, Socket::ERROR_NOROUTETOHOST);
		}
	}else{
 		Packet* packet = new Packet();
		*packet = *p;
	//	NS_LOG_DEBUG("Got packet: " << *p <<"\n");


		UdpHeader udpHeader;
		WprWaypointHeader wwHeader;
		wwHeader.SetReverse(false);
		packet->RemoveHeader (udpHeader);
		packet->RemoveHeader (wwHeader);
	//	NS_LOG_DEBUG("wwHeader: " << wwHeader);

		NS_ASSERT(wwHeader.GetWaypoint().GetHops() > -1);
		NS_ASSERT(wwHeader.GetSrc().GetHops() > -1);

		if(!m_withEntryPoint){
			Ipv4Address empty;
			NS_ASSERT(wwHeader.GetWaypoint().GetEntryPoint().GetHops() == 0);
			NS_ASSERT(wwHeader.GetSrc().GetEntryPoint().GetHops()  == 0);
			NS_ASSERT(wwHeader.GetWaypoint().GetEntryPoint().GetAddress() == empty);
			NS_ASSERT(wwHeader.GetSrc().GetEntryPoint().GetAddress()  == empty);
		}else{
			NS_ASSERT(wwHeader.GetWaypoint().GetEntryPoint().GetHops() > -1);
			NS_ASSERT(wwHeader.GetSrc().GetEntryPoint().GetHops() > -1);
			NS_ASSERT(wwHeader.GetWaypoint().GetHops() >= wwHeader.GetWaypoint().GetEntryPoint().GetHops());
			NS_ASSERT(wwHeader.GetSrc().GetHops() >= wwHeader.GetSrc().GetEntryPoint().GetHops());
		}
	
		Waypoint srcNode = wwHeader.GetSrc();
		Neighbor previousHop;
	        if(!srcNode.GetTrace().empty()){
        	        previousHop = srcNode.GetTrace().back();
	        }else{
	                previousHop = srcNode;
	        }
		if(!wwHeader.GetReverse()){
//			std::cerr << "srcNode: " << srcNode << "\n";
			wwHeader.IncreaseMetric(CalculateDistance(m_position, previousHop.GetPosition()));
			srcNode = wwHeader.GetSrc();
//			std::cerr << "srcNode after: " << srcNode << "\n";
		}

		if(wwHeader.GetReverse()){
			Ipv4Address myAddr = m_socketAddresses.begin()->second.GetLocal();
			Neighbor srcEntry = srcNode.GetEntryPoint();
			if(myAddr == srcEntry.GetAddress()){
				NS_LOG_DEBUG("I'm the src entry point in the packet - removing");
				Ipv4Address empty;
				srcEntry.SetAddress(empty);
				srcEntry.SetHops(0);
				srcEntry.SetDist(0);
				srcNode.SetEntryPoint(srcEntry);
				wwHeader.SetSrc(srcNode);
				
			}
//			NS_LOG_ERROR("Got reversed packet - decrease hops");
			//Need to reverse previous increase
//			wwHeader.DecreaseMetric(0);
//			wwHeader.DecreaseMetric(0);
		}else{
			NS_LOG_INFO("Increase hops");
			NS_ASSERT(srcNode.GetHops() > 0);
			updateHeader(wwHeader, iface.GetLocal());
			srcNode = wwHeader.GetSrc();	
			/*bool result =*/ m_neighborTable.AddWaypoint (srcNode,  m_position);
		//	result++;
       		        //if(result) NS_LOG_WARN("Added waypoint: "<< srcNode  << m_neighborTable << "\n");
			NS_LOG_INFO("header after upgrade:" << wwHeader);
		}
	
		//if(p->GetUid() == 8331){
		//}
		if (m_ipv4->IsDestinationAddress (dst, iif)){
			NS_LOG_INFO("Packet for me!");
			NS_LOG_ERROR("#" << p->GetUid () << " " << iface.GetLocal () << "("<< packet->GetUid()<< ")");
//			NS_LOG_DEBUG("Got waypoint header: " << wwHeader << "\n");
			//NS_LOG_DEBUG(m_neighborTable);
			NS_LOG_INFO ("Unicast local delivery to " << dst << " packet: " << *packet);
			packet->AddHeader(udpHeader);
		        lcb (packet, header, iif);
		}else{
			NS_LOG_INFO("Packet to forward");
			//add myself to the trace		
			
			Neighbor me(m_position, iface.GetLocal());
			srcNode.UpdateTrace(me);
			wwHeader.SetSrc(srcNode);

			//NS_LOG_WARN("after src update " << wwHeader);
			Neighbor target;
			Ipv4Address gw;
			Ipv4Route* route = new Ipv4Route();
			Ptr<NetDevice> dev = m_ipv4->GetNetDevice (m_ipv4->GetInterfaceForAddress (iface.GetLocal ()));
			route->SetDestination (dst);
		        route->SetSource (origin);
		        route->SetOutputDevice (dev);
			

			if(allNodesTable.LookupNeighbor (dst, target)){
				NS_LOG_INFO( "Translated " << dst << " to " << target.GetPosition() << "\n");
				if( !ChooseGateway(target, wwHeader, gw, *packet) ){
					//delete self from the trace
					wwHeader.PopSrcTrace(gw);
					if(!wwHeader.PopSrcTrace(gw)){
						if(m_withReversePacket && m_neighborTable.LookupNeighbor (origin)){
							NS_LOG_DEBUG("Couldn't find a route, sending to origin with reverse flag to " <<  origin);
							wwHeader.DecreaseMetric(CalculateDistance(m_position, previousHop.GetPosition()));
	                                                route->SetGateway(origin);
        	                                        wwHeader.SetReverse(true);
						}else{
							NS_LOG_ERROR("No route to host " << p->GetUid());
							NS_LOG_WARN (m_neighborTable);
							droped.push_back(p->GetUid());
							NS_LOG_WARN (wwHeader);
							return false;
						}
					}else{
						if(m_withReversePacket){
							NS_LOG_DEBUG("Couldn't find a route, sending to previous hop with reverse flag to " <<  gw);
							wwHeader.DecreaseMetric(CalculateDistance(m_position, previousHop.GetPosition()));
							route->SetGateway(gw);
							wwHeader.SetReverse(true);
						}
					}
				}else{
					//NS_LOG_DEBUG("wwHeader after choosing Gateway: " << wwHeader);
					//NS_LOG_DEBUG("caching packet: " << packet->GetUid() << " with gw: " << gw);
					m_neighborTable.CachePacket(*packet, gw);
					route->SetGateway(gw);
					wwHeader.SetReverse(false);
				}

			}else{
				NS_LOG_ERROR("Couldn't find a node with IP: " << dst << "\n");
				return false;
			}
		
			if(CheckOff(gw)){
			        NS_LOG_DEBUG(route->GetGateway() << ":host is down!");
				m_neighborTable.PurgeNode(route->GetGateway());
			        return false;
			}
	
			NS_LOG_DEBUG("New packet being forwarded: " << wwHeader);
			
			packet->AddHeader (wwHeader);
			packet->AddHeader (udpHeader);
	          	ucb (route,packet,header);
			NS_LOG_ERROR("?" << p->GetUid () << " " << iface.GetLocal () << "("<< packet->GetUid() <<")(" << ")->" << gw <<"->" << dst);
	          	return true;
		}
	}

  return true;
}

void 
RoutingProtocol::updateHeader(WprWaypointHeader & wwHeader, Ipv4Address myAddr)
{

	Waypoint srcNode = wwHeader.GetSrc();
	Ipv4Address empty;
	Neighbor previousHop;
	if(!srcNode.GetTrace().empty()){
		previousHop = srcNode.GetTrace().back();
	}else{
		previousHop = srcNode;
	}
	if(m_withEntryPoint){
		Neighbor entryPoint = srcNode.GetEntryPoint();
		NS_LOG_INFO("Previous hop:"<< previousHop << " entryPoint:" << entryPoint);
		int hopIndex = m_neighborTable.GetWaypointIndex(previousHop.GetPosition());
		int entryIndex = m_neighborTable.GetWaypointIndex(entryPoint.GetPosition());
		NS_LOG_INFO("Previous hop index:"<< hopIndex << " entryPoint index:" << entryIndex);
		if((entryPoint.GetAddress() == empty)  || ((entryIndex/4) == (hopIndex/4))){
			NS_LOG_DEBUG("Replacing trace entry point");
			previousHop.SetHops(1);
			previousHop.SetDist(CalculateDistance(previousHop.GetPosition(), m_position));
			NS_LOG_DEBUG("Calculated distance between " << previousHop.GetPosition() << " and " << m_position << " = " << CalculateDistance(previousHop.GetPosition(), m_position));
			srcNode.SetEntryPoint(previousHop);
			NS_LOG_DEBUG("New one: " << srcNode);
			wwHeader.SetSrc(srcNode);
		}
	}

}


bool 
RoutingProtocol::ChooseGateway(Neighbor dst, WprWaypointHeader & wwHeader, Ipv4Address & gw, Packet packet)
{

//	NS_LOG_ERROR(m_neighborTable);

	//NS_LOG_DEBUG("choose gw: " << wwHeader);

	Neighbor neighbor;
	Waypoint myWaypoint;
	Waypoint headerWaypoint = wwHeader.GetWaypoint();
	Neighbor target = dst;
	Ipv4Address myAddr = m_socketAddresses.begin()->second.GetLocal();
	Ipv4Address empty;

	Waypoint srcNode = wwHeader.GetSrc();
	Neighbor previousHop;
//	std::cout << "srcNode:" << srcNode << "\n";
	if(srcNode.GetTrace().size() > 1){
		previousHop = srcNode.GetTrace()[srcNode.GetTrace().size() - 2];
	}else{
		previousHop = srcNode;
	}
//	std::cout << "myaddr:" << myAddr << " previous:" << previousHop.GetAddress() << "\n";


if(m_withWaypoint){
	if((headerWaypoint.GetAddress() == myAddr) || (wwHeader.GetReverse())){
		NS_LOG_WARN("I'm the waypoint - removing");
		//empty
		myWaypoint.SetHops(0);
		std::vector<Neighbor> emptyVector;
		myWaypoint.SetTrace(emptyVector);
		wwHeader.SetWaypoint(myWaypoint);
	}
if(m_withEntryPoint){
	Neighbor hEntryPoint = headerWaypoint.GetEntryPoint();
	if(hEntryPoint.GetAddress() == myAddr){
		NS_LOG_DEBUG("I'm(" << myAddr <<") the entry point, zeroing hops\n");
		Ipv4Address empty;
		hEntryPoint.SetAddress(empty);
		hEntryPoint.SetHops(0);
		headerWaypoint.SetEntryPoint(hEntryPoint);
		wwHeader.SetWaypoint(headerWaypoint);
	}
} /* WITH_ENTRY_POINT */
} /*WITH_WAYPOINT */

	if(m_neighborTable.LookupNeighbor (dst.GetAddress(), target)){
		NS_LOG_WARN("Target is my neighbor - sending directly\n");
		gw = dst.GetAddress();
		wwHeader.SetExplore(0);
		return true;
	}
	
if(m_withWaypoint){
	if(m_neighborTable.LookupClosestWaypoint(target.GetPosition(), headerWaypoint,  myWaypoint, packet))
	{
		NS_LOG_WARN("Got a waypoint for this destination " << myWaypoint);
		if(myWaypoint.IsBetterWaypointThan(target, headerWaypoint))
		{
			NS_LOG_WARN("My waypoint is better that the one from the packet - replacing\n");
			wwHeader.SetWaypoint(myWaypoint);
		}
	}

	else if(m_withEntryPoint){
		if(m_neighborTable.ImproveEntryPoint(headerWaypoint)){
			NS_LOG_WARN("Found another waypoint with the same entry point - updating trace");
			wwHeader.SetWaypoint(headerWaypoint);
		}
	}
				


	if((wwHeader.GetWaypoint().GetHops()) && (m_neighborTable.LookupNeighbor (wwHeader.GetWaypoint().GetAddress(), target))){
		NS_LOG_WARN("Waypoint is my neighbor - sending directly\n");
		wwHeader.DecreaseWaypointHops();
		gw = target.GetAddress();
		wwHeader.SetExplore(0);
		return true;
	}
	

	
/*	if(m_neighborTable.LookupNodeInTrace (target.GetAddress(), myWaypoint)){
		wwHeader.SetWaypoint(myWaypoint);
		NS_LOG_ERROR("Found target in trace or as a waypoint");
	}*/
	if(wwHeader.PopWaypointTrace(gw))
	{
		if(gw == myAddr){
			NS_LOG_WARN("poped myself from trace");
		}else{
			wwHeader.DecreaseWaypointHops();
			NS_LOG_WARN("New gateway poped from trace: " << gw);
			NS_LOG_WARN("Waypoint: " << wwHeader.GetWaypoint().GetAddress());
			wwHeader.SetExplore(0);
			return true;
		}
	}else{
		NS_LOG_ERROR("No trace for the waypoint");
		NS_LOG_DEBUG("header: " << wwHeader);
	}	
} /* WITH_WAYPOINT */

if(m_withWaypoint){
if(m_withEntryPoint){
	headerWaypoint = wwHeader.GetWaypoint();
	Neighbor entryPoint = headerWaypoint.GetEntryPoint();
	if((entryPoint.GetHops() > 0) && (entryPoint.GetAddress() != empty) && (entryPoint.GetAddress() != myAddr)){
		NS_LOG_WARN("Setting entry point as greedy routing target" << entryPoint.GetAddress());
		target = entryPoint;
	}else{
		entryPoint.SetAddress(empty);
		headerWaypoint.SetEntryPoint(entryPoint);
		wwHeader.SetWaypoint(headerWaypoint);
	}
} /* WITH_ENTRY_POINT */
} /* WITH_WAYPOINT */


	if(m_neighborTable.LookupClosestNeighbor(target.GetPosition(), neighbor, packet, previousHop))
	{
		NS_LOG_INFO("Closest neighbor: " << neighbor);
		double myDist;
		if(!wwHeader.GetExplore()){
			myDist = CalculateDistance(m_position, target.GetPosition());
		}else{
			Neighbor exploreNode = srcNode.GetTrace()[srcNode.GetTrace().size() - (wwHeader.GetExplore())];
			myDist = CalculateDistance(exploreNode.GetPosition(), target.GetPosition()); 
		}
		double wDist = CalculateDistance(neighbor.GetPosition(), target.GetPosition());
		if(myDist < wDist){
			if( (!m_withExplore) || (wwHeader.GetExplore() >= MAX_TRACE) ){
				NS_LOG_ERROR("Can't find neighbor closer then me");
				//NS_LOG_ERROR(m_neighborTable);
				return false;
			}else{
				NS_LOG_WARN(" Greedy routing sending explore packet to : " << neighbor.GetAddress() << "\n");
				NS_LOG_DEBUG("header: " << wwHeader);
				wwHeader.SetExplore(wwHeader.GetExplore() + 1);
				gw = neighbor.GetAddress();
				return true;
			}
		}else{
			NS_LOG_WARN(" Greedy routing sending packet to : " << neighbor.GetAddress() << "\n");
			NS_LOG_DEBUG("header: " << wwHeader);
			gw = neighbor.GetAddress();
			wwHeader.SetExplore(0);
			return true;
		}
	}else{
	   	NS_LOG_ERROR(" Couldn't find closest neighbor\n");
		return false;
	}
	NS_LOG_ERROR("Couldn't set the gateway");
	return false;
}

void
RoutingProtocol::RecvWpr (Ptr<Socket> socket)
{
  Address sourceAddress;
  Ptr<Packet> advpacket = Create<Packet> ();
  Ptr<Packet> packet = socket->RecvFrom (sourceAddress);
  uint32_t packetSize = packet->GetSize ();
  InetSocketAddress inetSourceAddr = InetSocketAddress::ConvertFrom (sourceAddress);
  Ipv4Address sender = inetSourceAddr.GetIpv4 ();
  NS_LOG_LOGIC (m_mainAddress << " received wpr packet of size: " << packetSize
                                 << " and packet id: " << packet->GetUid ());

  WprHeader wprHeader;
  packet->RemoveHeader (wprHeader);
//  NS_LOG_LOGIC( "WPR packet: " << wprHeader << "\n");
  Neighbor n (wprHeader.GetPos(), sender);
  NS_LOG_LOGIC("got wpr packet from " << sender);
  m_neighborTable.AddNeighbor (n);
 // allNodesTable.AddNeighbor (n);
 // NS_LOG_DEBUG(m_neighborTable);
  

}

void
RoutingProtocol::SendPeriodicUpdate ()
{

  std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin ();
  Ipv4InterfaceAddress iface = j->second;
  Ipv4Address addr = iface.GetLocal ();

  if(CheckOff(addr)){
        NS_LOG_DEBUG( "I'm innactive don't send periodic updates!\n");
        return;
  }

  NS_LOG_LOGIC (m_mainAddress << " is sending out its periodic update");
  for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j
       != m_socketAddresses.end (); ++j)
    {
     
 
      Ptr<Socket> socket = j->first;
      Ipv4InterfaceAddress iface = j->second;
      Ptr<Packet> packet = Create<Packet> ();
      WprHeader wprHeader;
      wprHeader.SetPos (m_position);
      packet->AddHeader (wprHeader);
      
      NS_LOG_LOGIC ("Forwarding details are, my position: " << wprHeader.GetPos ());
      socket->Send (packet);   
      Ipv4Address destination;
      //destination = Ipv4Address ("10.1.1.255");
      destination =  iface.GetBroadcast ();
      socket->SendTo (packet, 0, InetSocketAddress (destination, WPR_PORT));
      NS_LOG_FUNCTION ("PeriodicUpdate Packet UID is : " << packet->GetUid ());
    }
   if(m_counter){
	m_periodicUpdateTimer.Schedule (m_periodicUpdateInterval + MicroSeconds (25 * m_uniformRandomVariable->GetInteger (0,1000)));
	m_counter--;
   }
}

void
RoutingProtocol::SetIpv4 (Ptr<Ipv4> ipv4)
{
  NS_ASSERT (ipv4 != 0);
  NS_ASSERT (m_ipv4 == 0);
  m_ipv4 = ipv4;
  // Create lo route. It is asserted that the only one interface up for now is loopback
  NS_ASSERT (m_ipv4->GetNInterfaces () == 1 && m_ipv4->GetAddress (0, 0).GetLocal () == Ipv4Address ("127.0.0.1"));
  m_lo = m_ipv4->GetNetDevice (0);
  NS_ASSERT (m_lo != 0);
  // Remember lo route
  Simulator::ScheduleNow (&RoutingProtocol::Start,this);
}

void
RoutingProtocol::NotifyInterfaceUp (uint32_t i)
{
  NS_LOG_FUNCTION (this << m_ipv4->GetAddress (i, 0).GetLocal ()
                        << " interface is up");
  Ptr<Ipv4L3Protocol> l3 = m_ipv4->GetObject<Ipv4L3Protocol> ();
  Ipv4InterfaceAddress iface = l3->GetAddress (i,0);
  if (iface.GetLocal () == Ipv4Address ("127.0.0.1"))
    {
      return;
    }
  // Create a socket to listen only on this interface
  Ptr<Socket> socket = Socket::CreateSocket (GetObject<Node> (),UdpSocketFactory::GetTypeId ());
  NS_ASSERT (socket != 0);
  socket->SetRecvCallback (MakeCallback (&RoutingProtocol::RecvWpr,this));
  socket->BindToNetDevice (l3->GetNetDevice (i));
  socket->Bind (InetSocketAddress (Ipv4Address::GetAny (), WPR_PORT));
  socket->SetAllowBroadcast (true);
  socket->SetAttribute ("IpTtl",UintegerValue (1));
  m_socketAddresses.insert (std::make_pair (socket,iface));
  // Add local broadcast record to the routing table
  Ptr<NetDevice> dev = m_ipv4->GetNetDevice (m_ipv4->GetInterfaceForAddress (iface.GetLocal ()));
  if (m_mainAddress == Ipv4Address ())
    {
      m_mainAddress = iface.GetLocal ();
    }
  NS_ASSERT (m_mainAddress != Ipv4Address ());
}

void
RoutingProtocol::NotifyInterfaceDown (uint32_t i)
{
  Ptr<Ipv4L3Protocol> l3 = m_ipv4->GetObject<Ipv4L3Protocol> ();
  Ptr<NetDevice> dev = l3->GetNetDevice (i);
  Ptr<Socket> socket = FindSocketWithInterfaceAddress (m_ipv4->GetAddress (i,0));
  NS_ASSERT (socket);
  socket->Close ();
  m_socketAddresses.erase (socket);
  if (m_socketAddresses.empty ())
    {
      NS_LOG_LOGIC ("No wpr interfaces");
      return;
    }
}

void
RoutingProtocol::NotifyAddAddress (uint32_t i,
                                   Ipv4InterfaceAddress address)
{
  NS_LOG_FUNCTION (this << " interface " << i << " address " << address);
  Ptr<Ipv4L3Protocol> l3 = m_ipv4->GetObject<Ipv4L3Protocol> ();
  if (!l3->IsUp (i))
    {
      return;
    }
  Ipv4InterfaceAddress iface = l3->GetAddress (i,0);
  Ptr<Socket> socket = FindSocketWithInterfaceAddress (iface);
  if (!socket)
    {
      if (iface.GetLocal () == Ipv4Address ("127.0.0.1"))
        {
          return;
        }
      Ptr<Socket> socket = Socket::CreateSocket (GetObject<Node> (),UdpSocketFactory::GetTypeId ());
      NS_ASSERT (socket != 0);
      socket->SetRecvCallback (MakeCallback (&RoutingProtocol::RecvWpr,this));
      socket->BindToNetDevice (l3->GetNetDevice (i));
      // Bind to any IP address so that broadcasts can be received
      socket->Bind (InetSocketAddress (Ipv4Address::GetAny (), WPR_PORT));
      socket->SetAllowBroadcast (true);
      m_socketAddresses.insert (std::make_pair (socket,iface));
      Ptr<NetDevice> dev = m_ipv4->GetNetDevice (m_ipv4->GetInterfaceForAddress (iface.GetLocal ()));
    }
}

void
RoutingProtocol::NotifyRemoveAddress (uint32_t i,
                                      Ipv4InterfaceAddress address)
{
  Ptr<Socket> socket = FindSocketWithInterfaceAddress (address);
  if (socket)
    {
      m_socketAddresses.erase (socket);
      Ptr<Ipv4L3Protocol> l3 = m_ipv4->GetObject<Ipv4L3Protocol> ();
      if (l3->GetNAddresses (i))
        {
          Ipv4InterfaceAddress iface = l3->GetAddress (i,0);
          // Create a socket to listen only on this interface
          Ptr<Socket> socket = Socket::CreateSocket (GetObject<Node> (),UdpSocketFactory::GetTypeId ());
          NS_ASSERT (socket != 0);
          socket->SetRecvCallback (MakeCallback (&RoutingProtocol::RecvWpr,this));
          // Bind to any IP address so that broadcasts can be received
          socket->Bind (InetSocketAddress (Ipv4Address::GetAny (), WPR_PORT));
          socket->SetAllowBroadcast (true);
          m_socketAddresses.insert (std::make_pair (socket,iface));
        }
    }
}

Ptr<Socket>
RoutingProtocol::FindSocketWithInterfaceAddress (Ipv4InterfaceAddress addr) const
{
  for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j
       != m_socketAddresses.end (); ++j)
    {
      Ptr<Socket> socket = j->first;
      Ipv4InterfaceAddress iface = j->second;
      if (iface == addr)
        {
          return socket;
        }
    }
  Ptr<Socket> socket;
  return socket;
}

void
RoutingProtocol::Send (Ptr<Ipv4Route> route,
                       Ptr<const Packet> packet,
                       const Ipv4Header & header)
{

  /*if(CheckOff(header.GetDestination())){
	std::cout << "Host is down!\n";
	return;
  }*/
  Ptr<Ipv4L3Protocol> l3 = m_ipv4->GetObject<Ipv4L3Protocol> ();
  NS_ASSERT (l3 != 0);
  Ptr<Packet> p = packet->Copy ();
  l3->Send (p,route->GetSource (),header.GetDestination (),header.GetProtocol (),route);
}


}
}
