/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2010 Hemanth Narra
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Hemanth Narra <hemanth@ittc.ku.com>
 *
 * James P.G. Sterbenz <jpgs@ittc.ku.edu>, director
 * ResiliNets Research Group  http://wiki.ittc.ku.edu/resilinets
 * Information and Telecommunication Technology Center (ITTC)
 * and Department of Electrical Engineering and Computer Science
 * The University of Kansas Lawrence, KS USA.
 *
 * Work supported in part by NSF FIND (Future Internet Design) Program
 * under grant CNS-0626918 (Postmodern Internet Architecture),
 * NSF grant CNS-1050226 (Multilayer Network Resilience Analysis and Experimentation on GENI),
 * US Department of Defense (DoD), and ITTC at The University of Kansas.
 */

#ifndef WPR_PACKET_H
#define WPR_PACKET_H

#include <iostream>
#include "ns3/header.h"
#include "ns3/ipv4-address.h"
#include "ns3/nstime.h"
#include "ns3/vector.h"
#include "wpr-waypoint.h"
#include "wpr-common.h"

#include "ns3/tag.h"
#include "ns3/packet.h"
#include "ns3/uinteger.h"

namespace ns3 {
namespace wpr {
/**
 * \ingroup wpr
 * \brief WPR Update Packet Format
 * \verbatim
 |      0        |      1        |      2        |       3       |
  0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 |                      Destination Address                      |
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 |                            HopCount                           |
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 |                       Sequence Number                         |
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * \endverbatim
 */

class WprHeader : public Header
{
public:
  WprHeader (Vector pos = Vector () );
  virtual ~WprHeader ();
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;
  virtual uint32_t GetSerializedSize () const;
  virtual void Serialize (Buffer::Iterator start) const;
  virtual uint32_t Deserialize (Buffer::Iterator start);
  virtual void Print (std::ostream &os) const;

  void
  SetPos (Vector pos)
  {
    m_pos = pos;
  }
  Vector
  GetPos () const
  {
    return m_pos;
  }
private:
  Vector m_pos;
};
static inline std::ostream & operator<< (std::ostream& os, const WprHeader & packet)
{
  packet.Print (os);
  return os;
}

class WprWaypointHeader : public Header
{
public:
  WprWaypointHeader (Waypoint waypoint = Waypoint () );
  virtual ~WprWaypointHeader ();
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;
  virtual uint32_t GetSerializedSize () const;
  virtual void Serialize (Buffer::Iterator start) const;
  virtual uint32_t Deserialize (Buffer::Iterator start);
  virtual void Print (std::ostream &os) const;
  void
  SetWaypoint (Waypoint waypoint)
  {
    m_waypoint = waypoint;
  }
  void
  SetSrc (Waypoint src)
  {
    m_src = src;
  }
  Waypoint
  GetWaypoint () const
  {
    return m_waypoint;
  }
  Waypoint
  GetSrc () const
  {
    return m_src;
  }
  void
  SetReverse(bool reverse){
    m_reverse = reverse;
  }
  bool
  GetReverse(){
    return m_reverse;
  }
  void
  SetExplore(int explore){
    m_explore = explore;
  }
  bool
  GetExplore(){
    return m_explore;
  }
  bool
  PopWaypointTrace(Ipv4Address & w);
  bool
  PopSrcTrace(Ipv4Address & w);

  void 
  IncreaseMetric(double pos);

  void
  DecreaseMetric(double pos);

  void
  DecreaseWaypointHops();



private:
  Waypoint m_waypoint;
  Waypoint m_src;
  bool m_reverse;
  int m_explore;

};

static inline std::ostream & operator<< (std::ostream& os, const WprWaypointHeader & packet)
{
  packet.Print (os);
  return os;
}



class ReverseTag : public Tag
{
public:
  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;
  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (TagBuffer i) const;
  virtual void Deserialize (TagBuffer i);
  virtual void Print (std::ostream &os) const;
  
  void SetCount(uint32_t count);
  uint32_t GetCount();
  void SetForward(bool forward);
  bool GetForward();
private:
  uint32_t  m_count;
  bool m_forward;
};


}
}

#endif /* WPR_PACKET_H */
