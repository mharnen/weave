#ifndef WPR_NTABLE_H
#define WPR_NTABLE_H

#include <cassert>
#include <map>
#include <sys/types.h>
#include "ns3/ipv4.h"
#include "ns3/ipv4-route.h"
#include "ns3/timer.h"
#include "ns3/net-device.h"
#include "ns3/output-stream-wrapper.h"
#include "ns3/vector.h"
#include "ns3/packet.h"
#include "wpr-common.h"


//#define AX 200
//#define AY 200
extern double ax;
extern double ay;
extern double az;

namespace ns3 {
namespace wpr {

class Neighbor
{
public:
  Neighbor (Vector pos = Vector(),
                     Ipv4Address  addr = Ipv4Address ());

  ~Neighbor ();

void SetPosition(Vector pos)
{
	m_pos = pos;
}
Vector GetPosition() const
{
	return m_pos;
}

Ipv4Address GetAddress () const
{
    return m_addr;
}

void
SetAddress (Ipv4Address addr)
{
    m_addr = addr;
}

int GetHops () const
{
    return m_hops;
}

void
SetHops (int hops)
{
    m_hops = hops;
}


double
  GetDist() const {
        return m_dist;
  }
void
  SetDist(double dist){
        m_dist = dist;
}


void
  Print (Ptr<OutputStreamWrapper> stream) const;

void
  Print (std::ostream &os) const;
void 
  IncreaseHops(int inc);

protected:
  Vector m_pos;
  Ipv4Address m_addr;
  int m_hops;
  double m_dist;
};


static inline std::ostream & operator<< (std::ostream& os, const Neighbor & n)
{
  n.Print (os);
  return os;
}


class NeighborTable
{
public:
  NeighborTable (Vector pos = Vector());
  ~NeighborTable();
  bool
  AddNeighbor (Neighbor & n);
  bool
  DeleteNeighbor (Ipv4Address n);
  bool
  LookupNeighbor (Ipv4Address dst, Neighbor & n);
  bool
  LookupNeighbor(int id, Neighbor & n);
  bool
  LookupNeighbor (Ipv4Address dst);
  bool
  LookupClosestNeighbor (Vector pos, Neighbor & n, Packet packet, Neighbor previousHop);
  uint32_t
  NeighborTableSize ();
  double GetDiameter() const
  {
	return m_diameter;
  }
 

  void SetPosition(Vector pos)
  {
  	m_pos = pos;
  }
  Vector GetPosition() const
  {
  	return m_pos;
  }

  bool
  CachePacket(Packet packet, Ipv4Address addr);
  bool
  PacketInCache(Packet packet, Ipv4Address addr);

  void
  Print (Ptr<OutputStreamWrapper> stream) const;
  void
  Print (std::ostream &os) const;


protected:
  void CalculateDiameter();
  void UpdateSpaces();

  short m_spaces[MAX_SPACES_DEPTH * SPACES_DIVISION];
  double m_diameter;
  Vector m_pos;
  std::map<Ipv4Address, Neighbor> m_ipv4AddressEntry;

};
 static inline std::ostream & operator<< (std::ostream& os, const NeighborTable & t)
  {
    t.Print(os);
    return os;
  }
}
}
#endif /* WPR_NTABLE_H */

