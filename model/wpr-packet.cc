/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2010 Hemanth Narra
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Hemanth Narra <hemanth@ittc.ku.com>
 *
 * James P.G. Sterbenz <jpgs@ittc.ku.edu>, director
 * ResiliNets Research Group  http://wiki.ittc.ku.edu/resilinets
 * Information and Telecommunication Technology Center (ITTC)
 * and Department of Electrical Engineering and Computer Science
 * The University of Kansas Lawrence, KS USA.
 *
 * Work supported in part by NSF FIND (Future Internet Design) Program
 * under grant CNS-0626918 (Postmodern Internet Architecture),
 * NSF grant CNS-1050226 (Multilayer Network Resilience Analysis and Experimentation on GENI),
 * US Department of Defense (DoD), and ITTC at The University of Kansas.
 */
#include "wpr-packet.h"
#include "ns3/address-utils.h"
#include "ns3/packet.h"
#include "ns3/log.h"

NS_LOG_COMPONENT_DEFINE ("WprPacket");

namespace ns3 {
namespace wpr {
NS_OBJECT_ENSURE_REGISTERED (WprHeader);
WprHeader::WprHeader (Vector pos)
  : m_pos (pos)
{
}

WprHeader::~WprHeader ()
{
}

TypeId
WprHeader::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::wpr::WprHeader")
    .SetParent<Header> ()
    .AddConstructor<WprHeader> ();
  return tid;
}

TypeId
WprHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

uint32_t
WprHeader::GetSerializedSize () const
{
  return sizeof(Vector);
}

void
WprHeader::Serialize (Buffer::Iterator i) const
{
  i.WriteHtonU64 (m_pos.x);
  i.WriteHtonU64 (m_pos.y);
  i.WriteHtonU64 (m_pos.z);

}

uint32_t
WprHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;

  m_pos.x = i.ReadNtohU64 ();
  m_pos.y = i.ReadNtohU64 ();
  m_pos.z = i.ReadNtohU64 ();

  uint32_t dist = i.GetDistanceFrom (start);
  NS_ASSERT (dist == GetSerializedSize ());
  return dist;
}

void
WprHeader::Print (std::ostream &os) const
{
  os << "Position: " << m_pos;
}


NS_OBJECT_ENSURE_REGISTERED (WprWaypointHeader);
WprWaypointHeader::WprWaypointHeader (Waypoint waypoint)
  : m_waypoint (waypoint)
{
}

WprWaypointHeader::~WprWaypointHeader ()
{
}

TypeId
WprWaypointHeader::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::wpr::WprWaypointHeader")
    .SetParent<Header> ()
    .AddConstructor<WprWaypointHeader> ();
  return tid;
}

TypeId
WprWaypointHeader::GetInstanceTypeId () const
{
  return GetTypeId ();
}

uint32_t
WprWaypointHeader::GetSerializedSize () const
{
/*  std::cout << "Calculating size\t trace size:" << m_waypoint.GetTrace().size() << " neighbor size:" << sizeof(Neighbor) << " sizeof hops: " << sizeof(m_waypoint.GetHops()) \
            << " vector:" << sizeof(Vector) << " ipv4addr:" << sizeof(Ipv4Address) <<"\n";*/
  return ((4 + (m_waypoint.GetTrace().size() + 2) * (sizeof(Vector) + sizeof(Ipv4Address)))  + \
	 (4 + (m_src.GetTrace().size() + 2) *  (sizeof(Vector) + sizeof(Ipv4Address))) + \
         4 * sizeof(m_waypoint.GetHops()) + sizeof(m_reverse) + 4*sizeof(double));
}

void
WprWaypointHeader::Serialize (Buffer::Iterator i) const
{
  NS_LOG_INFO( "Serializing waypoint " << m_waypoint);
  WriteTo(i, m_waypoint.GetAddress());
  i.WriteHtonU64 (m_waypoint.GetPosition().x);
  i.WriteHtonU64 (m_waypoint.GetPosition().y);
  i.WriteHtonU64 (m_waypoint.GetPosition().z);
  i.WriteHtonU32 (m_waypoint.GetHops());
  i.WriteHtonU64 (m_waypoint.GetDist());
  std::vector<Neighbor> trace = m_waypoint.GetTrace();
  i.WriteHtonU32 (trace.size());
  for(unsigned int j = 0; j < trace.size(); j++)
  {
	  WriteTo(i, trace[j].GetAddress());
	  i.WriteHtonU64 (trace[j].GetPosition().x);
	  i.WriteHtonU64 (trace[j].GetPosition().y);
	  i.WriteHtonU64 (trace[j].GetPosition().z);
  }

  NS_LOG_INFO("waypoint size trace: " << trace.size());
  Neighbor entryPoint = m_waypoint.GetEntryPoint();;
  WriteTo(i, entryPoint.GetAddress());
  i.WriteHtonU64 (entryPoint.GetPosition().x);
  i.WriteHtonU64 (entryPoint.GetPosition().y);
  i.WriteHtonU64 (entryPoint.GetPosition().z);
  i.WriteHtonU32 (entryPoint.GetHops());
  i.WriteHtonU64 (entryPoint.GetDist());
  
  NS_LOG_INFO("Serializing src " << m_src);
  WriteTo(i, m_src.GetAddress());
  i.WriteHtonU64 (m_src.GetPosition().x);
  i.WriteHtonU64 (m_src.GetPosition().y);
  i.WriteHtonU64 (m_src.GetPosition().z);
  i.WriteHtonU32 (m_src.GetHops());
  i.WriteHtonU64 (m_src.GetDist());
  trace = m_src.GetTrace();
  i.WriteHtonU32 (trace.size());
  for(unsigned int j = 0; j < trace.size(); j++)
  {
	  WriteTo(i, trace[j].GetAddress());
	  i.WriteHtonU64 (trace[j].GetPosition().x);
	  i.WriteHtonU64 (trace[j].GetPosition().y);
	  i.WriteHtonU64 (trace[j].GetPosition().z);
  }
  
  NS_LOG_INFO("src size trace: " << trace.size());
  entryPoint = m_src.GetEntryPoint();;
  WriteTo(i, entryPoint.GetAddress());
  i.WriteHtonU64 (entryPoint.GetPosition().x);
  i.WriteHtonU64 (entryPoint.GetPosition().y);
  i.WriteHtonU64 (entryPoint.GetPosition().z);
  i.WriteHtonU32 (entryPoint.GetHops());
  i.WriteHtonU64 (entryPoint.GetDist());
  
  i.WriteU8 (m_reverse);
}



uint32_t
WprWaypointHeader::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;
  Ipv4Address addr;
  ReadFrom(i, addr);
  m_waypoint.SetAddress(addr);

  m_waypoint.SetTimestamp(0);
  Vector pos;
  pos.x = i.ReadNtohU64 ();
  pos.y = i.ReadNtohU64 ();
  pos.z = i.ReadNtohU64 ();
  m_waypoint.SetPosition(pos);

  int hops = i.ReadNtohU32 ();
  m_waypoint.SetHops(hops);
  double dist = i.ReadNtohU64();
  m_waypoint.SetDist(dist);

  unsigned int sTrace = i.ReadNtohU32();
  NS_LOG_INFO("waypoint size trace: " << sTrace);
  Neighbor n;
  std::vector<Neighbor> trace;
  for(unsigned int j = 0; j < sTrace; j++)
  {
	  Ipv4Address addr;
          ReadFrom(i, addr);
	  n.SetAddress(addr);

	  Vector pos;
          pos.x = i.ReadNtohU64 ();
          pos.y = i.ReadNtohU64 ();
          pos.z = i.ReadNtohU64 ();
	  n.SetPosition (pos);
     	  trace.push_back(n);
  }
  m_waypoint.SetTrace(trace);


  Neighbor entryPoint;
  ReadFrom(i, addr);
  entryPoint.SetAddress(addr);

  pos.x = i.ReadNtohU64 ();
  pos.y = i.ReadNtohU64 ();
  pos.z = i.ReadNtohU64 ();
  entryPoint.SetPosition(pos);
  hops = i.ReadNtohU32 ();
  entryPoint.SetHops(hops);
  dist = i.ReadNtohU64();
  entryPoint.SetDist(dist);
  m_waypoint.SetEntryPoint(entryPoint);


  ReadFrom(i, addr);
  m_src.SetAddress(addr);
  m_src.SetTimestamp(0);
  pos.x = i.ReadNtohU64 ();
  pos.y = i.ReadNtohU64 ();
  pos.z = i.ReadNtohU64 ();
  m_src.SetPosition(pos);

  hops = i.ReadNtohU32 ();
  m_src.SetHops(hops);
  dist = i.ReadNtohU64 ();
  m_src.SetDist(dist);

  sTrace = i.ReadNtohU32();
  NS_LOG_INFO("src size trace: " << sTrace);
  trace.clear();
  for(unsigned int j = 0; j < sTrace; j++)
  {
	  Ipv4Address addr;
          ReadFrom(i, addr);
	  n.SetAddress(addr);

	  Vector pos;
          pos.x = i.ReadNtohU64 ();
          pos.y = i.ReadNtohU64 ();
          pos.z = i.ReadNtohU64 ();
	  n.SetPosition (pos);
     	  trace.push_back(n);
  }
  m_src.SetTrace(trace);
  
  ReadFrom(i, addr);
  entryPoint.SetAddress(addr);

  pos.x = i.ReadNtohU64 ();
  pos.y = i.ReadNtohU64 ();
  pos.z = i.ReadNtohU64 ();
  entryPoint.SetPosition(pos);
  hops = i.ReadNtohU32 ();
  entryPoint.SetHops(hops);
  dist = i.ReadNtohU64();
  entryPoint.SetDist(dist);
  m_src.SetEntryPoint(entryPoint);

  m_reverse = i.ReadU8();

  NS_LOG_INFO("Deserialized waypoint" << m_waypoint);
  NS_LOG_INFO("Deserialized src" << m_src);
  //std::cout << "Deserilized waypoint: " << m_waypoint << "\n";
  //std::cout << "size: " << GetSerializedSize () << " iterator: " << i.GetDistanceFrom (start) << "\n";
  uint32_t len = i.GetDistanceFrom (start);
  NS_ASSERT (len == GetSerializedSize ());
  return len;
}


void 
WprWaypointHeader::IncreaseMetric(double dist){
	NS_LOG_DEBUG("Increasing metric: " << m_src );
	m_src.IncreaseHops(1);
	m_src.SetDist(m_src.GetDist() + dist);
	Neighbor entryPoint = m_src.GetEntryPoint();
	Ipv4Address empty;
	if(entryPoint.GetAddress() != empty){
		entryPoint.IncreaseHops(1);
		entryPoint.SetDist(entryPoint.GetDist() + dist);
		m_src.SetEntryPoint(entryPoint);
	}
	NS_LOG_DEBUG("After increasing: " << m_src );

	
}

void 
WprWaypointHeader::DecreaseMetric(double dist){
	Ipv4Address empty;
	NS_LOG_DEBUG("decreasing metric: " << m_src );
	
	if(m_src.GetAddress() == empty){
		m_src.IncreaseHops(-1);
		m_src.SetDist(m_src.GetDist() - dist);
	}
	
	Neighbor entryPoint = m_src.GetEntryPoint();
	if(entryPoint.GetAddress() != empty){
		entryPoint.IncreaseHops(-1);
		entryPoint.SetDist(entryPoint.GetDist() - dist);
		m_src.SetEntryPoint(entryPoint);
	}
	NS_LOG_DEBUG( "After decreasing: " << m_src);
	
}
void 
WprWaypointHeader::DecreaseWaypointHops(){
	m_waypoint.SetHops(m_waypoint.GetHops() - 1);
	Neighbor entry = m_waypoint.GetEntryPoint();
	Ipv4Address empty;
	if(entry.GetAddress() != empty){
		entry.SetHops(entry.GetHops() -1);	
		m_waypoint.SetEntryPoint(entry);
	}
}
bool
WprWaypointHeader::PopWaypointTrace(Ipv4Address & addr)
  {
	return m_waypoint.PopTrace(addr);

  }

bool
WprWaypointHeader::PopSrcTrace(Ipv4Address & addr)
  {
	return m_src.PopTrace(addr);

  }

void
WprWaypointHeader::Print (std::ostream &os) const
{
  os << "Waypoint: " << m_waypoint << "\n Src:" << m_src;
}


TypeId
ReverseTag::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::wpr::ReverseTag")
    .SetParent<Tag> ()
    .AddConstructor<ReverseTag> ();
  return tid;
}

TypeId
ReverseTag::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}
uint32_t
ReverseTag::GetSerializedSize (void) const
{
  return 0;
}
void
ReverseTag::Serialize (TagBuffer i) const
{
}
void
ReverseTag::Deserialize (TagBuffer i)
{
}

void
ReverseTag::Print (std::ostream &os) const
{
  os << "ReverseTag";
}


void 
ReverseTag::SetCount(uint32_t count){
  m_count = count;
}

uint32_t 
ReverseTag::GetCount(){
  return m_count;
}

void 
ReverseTag::SetForward(bool forward){
  m_forward = forward;
}

bool 
ReverseTag::GetForward(){
  return m_forward;
}



}
}
