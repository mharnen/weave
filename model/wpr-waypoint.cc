#include "wpr-waypoint.h"
#include "ns3/simulator.h"
#include <iomanip>
#include <vector>
#include "ns3/log.h"


NS_LOG_COMPONENT_DEFINE ("WprWaypointTable");

namespace ns3 {
namespace wpr {


Waypoint::Waypoint (Vector pos, Ipv4Address addr, int hops, int timestamp, double dist)
  : Neighbor(pos, addr)
{
	m_hops = hops;
}

Waypoint::~Waypoint ()
{
}
WaypointTable::WaypointTable () 
{
}

WaypointTable::~WaypointTable ()
{
}

/*void 
Waypoint::ShiftTrace(){
}
*/
void 
Waypoint::SetTrace(std::vector<Neighbor> trace){
	m_trace = trace;
	NS_ASSERT(trace.size() <= MAX_TRACE);
}

bool 
Waypoint::PopTrace(Ipv4Address & addr)
{
	NS_ASSERT(m_trace.size() <= MAX_TRACE);
	if(! m_trace.empty()){
		std::vector<Neighbor>::iterator it;		
		addr = m_trace.back().GetAddress();
		m_trace.pop_back();
		return true;
	}
	return false;

}

bool 
Waypoint::GetNextHop(Ipv4Address & addr){
	NS_ASSERT(m_trace.size() <= MAX_TRACE);
        if(! m_trace.empty()){
                addr = m_trace.back().GetAddress();
                return true;
        }
        return false;

}
void
Waypoint::UpdateTrace(Neighbor n){
	NS_ASSERT(m_trace.size() <= MAX_TRACE);
        while ( !(m_trace.size() < MAX_TRACE)){
                m_trace.erase(m_trace.begin());
        }
        m_trace.push_back(n);
}

double
Waypoint::CalculateMetric(Vector pos){
//	std::cout << "dist/m_hops = " << m_dist / m_hops << " time: " << ((Simulator::Now().GetSeconds() - m_timestamp) / 10) << "\n";
//	return -(m_dist / m_hops);// + ((Simulator::Now().GetSeconds() - m_timestamp) / 10);
//	std::cout << "m_pos:" << m_pos << " pos:" << pos << " m_hops:" << m_hops << " result:" <<  CalculateDistance(m_pos, pos) / m_hops << "\n";
	return -(CalculateDistance(m_pos, pos) / m_hops);
}

bool
  Waypoint::IsBetterWaypointThan(Neighbor target, Waypoint w)
{
	Ipv4Address empty;
	if(w.GetAddress().IsEqual(empty)) return true;
	double myDist = CalculateDistance(m_pos, target.GetPosition());
	double hisDist = CalculateDistance(w.GetPosition(), target.GetPosition());
	if(myDist == hisDist) return (m_hops <= w.GetHops());
	return (myDist < hisDist);

}

bool
WaypointTable::LookupWaypoint (int index,
                           std::vector<Waypoint> & n)
{
  if (table.empty ())
    {
      return false;
    }
  std::map<int, std::vector<Waypoint> >::const_iterator i = table.find (index);
  if (i == table.end ())
    {
      return false;
    }

//  NS_ASSERT(i->second.m_trace.size() < i.second.MAX_TRACE);
  n = i->second;
  return true;
}

bool
WaypointTable::LookupClosestWaypoint (Vector pos, Waypoint waypoint, Waypoint & target, Packet packet)
{
	std::map<int, std::vector<Waypoint> >::iterator it;
	it = table.find(GetWaypointIndex(pos));
	bool flag = false;
	Ipv4Address empty;
	if((it == table.end()) && (waypoint.GetAddress() == empty)){
		NS_LOG_ERROR("No waypoint in this space");
		return false;
	}
	std::vector<Waypoint>::iterator iit;
	int minDist = -1;
	if(waypoint.GetAddress() != empty){
		NS_LOG_ERROR("but we had one in header" << waypoint);
		minDist = CalculateDistance(pos, waypoint.GetPosition());
	}
	for(it = table.begin(); it != table.end(); it++){
		for(iit = it->second.begin(); iit != it->second.end(); iit++){
			Ipv4Address nextHop;
			if( !(iit->GetNextHop(nextHop))){
				nextHop = iit->GetAddress();
			}
			if(PacketInCache(packet, nextHop)){
                        	NS_LOG_DEBUG("Already sent to this neighbor - skipping");
                         	continue;
			}

			NS_LOG_WARN("Got  minDist " <<  CalculateDistance(pos, iit->GetPosition()));
			if((CalculateDistance(pos, iit->GetPosition()) < minDist) || (minDist == -1)){
				NS_LOG_WARN("Better then minimum: " << minDist);
				minDist = CalculateDistance(pos, iit->GetPosition());
				target = *iit;
				flag = true;
			}
		}
		
	}
	return flag;

}


bool
WaypointTable::LookupNodeInTrace (Ipv4Address addr, Waypoint & waypoint){

	std::map<int, std::vector<Waypoint> >::iterator it;
	std::vector<Waypoint>::iterator iit;
	std::vector<Neighbor>::iterator iiit;
	for(it = table.begin(); it != table.end(); it++){
		for(iit = it->second.begin(); iit != it->second.end(); iit++){
			if(iit->GetAddress() == addr){
				std::cerr << "Found addr in Waypoint\n";
				waypoint = *iit;
				return true;
			}
			std::cerr << "Now looking in waypoint: " << *iit << "\n";
			std::vector<Neighbor> trace = iit->GetTrace();
			for(iiit = trace.begin(); iiit != trace.end(); iiit++){
				std::cerr << "Now looking in trace member: " << *iiit << "\n";
				if(iiit->GetAddress() == addr){
					std::cerr << "Found addr in trace!\n";
					waypoint = *iit;
					waypoint.SetAddress(iiit->GetAddress());
					return true;
				}
			}
		}
	}
	return false;
}

bool
WaypointTable::DeleteWaypoint (int index)
{
  if (table.erase (index) != 0)
    {
      return true;
    }
  return false;
}

bool
WaypointTable::AddWaypoint (Waypoint & n, Vector pos)
{
  Waypoint neighbor;
  if(LookupNeighbor (n.GetAddress(), neighbor)) return false;
  n.SetTimestamp(Simulator::Now().GetSeconds());

//  std::cout << "Timestamping new waypoint " << n.GetAddress() << " with: " << n.GetTimestamp() << "\n";
  std::cout << *this;
  std::map<int, std::vector<Waypoint> >::iterator i = table.find (GetWaypointIndex(n.GetPosition()));
  std::vector<Waypoint> v;
  if (i == table.end ()){
	v.push_back(n);
  	table.insert (std::make_pair (GetWaypointIndex(n.GetPosition()),v));
//	std::cout << "Empty space - adding waypoint \n";
	return true;
  }else{
	std::vector<Waypoint>::iterator vectorIt;
	for(vectorIt = i->second.begin(); vectorIt != i->second.end(); vectorIt++){
//		std::cout << "checking " << n.GetAddress() << " against " << vectorIt->GetAddress() << "\n";
		if(n.GetAddress() == vectorIt->GetAddress()){
//			std::cout << "checking " << n.CalculateMetric(pos) << " against " << vectorIt->CalculateMetric(pos) << "\n";
			if(n.CalculateMetric(pos) < vectorIt->CalculateMetric(pos)){
//				std::cout << "Replacing the same waypoint\n";
				i->second.erase(vectorIt);
		                i->second.push_back(n);
                		NS_LOG_WARN(*this);
		                return true;	
			}
		}
	}
	if(i->second.size() < MAX_WAYPOINTS){
//		std::cout << "Pushing waypoint on the next position\n";
		i->second.push_back(n);
		NS_LOG_WARN(*this);
		return true;
	}else{
		std::vector<Waypoint>::iterator worst;
		double worstMetric = -99999;
		for(vectorIt = i->second.begin(); vectorIt != i->second.end(); vectorIt++){
			if(worstMetric < vectorIt->CalculateMetric(pos)){
				worstMetric = vectorIt->CalculateMetric(pos);
				worst = vectorIt;
			}
                }
  //              std::cout << "Chosen worst metric:" << worstMetric << "node:" << *worst << "\n";
                if(n.CalculateMetric(pos) < worstMetric){
    //            	std::cout << "Replacing waypoint with lower metric\n";
                        i->second.erase(worst);
                        i->second.push_back(n);
                        NS_LOG_WARN(*this);
                        return true;
                }
		
        }
  }

  return false;
}
#if D3
int WaypointTable::GetWaypointIndex(Vector pos){
        int tx = ax;
        int ty = ay;
        int tz = az;

        int x = 0;
        int y = 0;
        int z = 0;

        int i = 0;
        int index;
        while( i < MAX_SPACES_DEPTH * SPACES_DIVISION){
                tx /= 2;
                ty /= 2;
                tz /= 2;
                if(pos.x < (x + tx)){
                        if(pos.y < (y + ty)){
                                if(pos.z < (z + tz)){
                                        index = 0;
                                }else{
                                        z += tz;
                                        index = 4;
                                }
                        }else{
                                if(pos.z < (z + tz)){
                                        index = 1;
                                }else{
                                        z +=tz;
                                        index = 5;
                                }
                                y += ty;
                        }
                }else{
                        if(pos.y < (y + ty)){
                                if(pos.z < (z + tz)){
                                        index = 2;
                                }else{
                                        index = 6;
                                        z += tz;
                                }
                        }else{
                                if(pos.z < (z + tz)){
                                        index = 3;
                                }else{
                                        index = 7;
                                        z += tz;
                                }
                                y += ty;
                        }
                        x += tx;
                }
                if(m_spaces[i] != index){
                        return i*6 + index;
                }
                i++;
        }
        return -1;
}
#else
int WaypointTable::GetWaypointIndex(Vector pos){
        //printf("MAX_WAYPOINTS %u\n", MAX_WAYPOINTS);
        int tx = ax;
        int ty = ay;

        int x = 0;
        int y = 0;

        int i = 0;
        int index;
        while( i < (MAX_SPACES_DEPTH * SPACES_DIVISION)){
                tx /= 2;
                ty /= 2;
                if(pos.x < (x + tx)){
                        if(pos.y < (y + ty)){
                                index = 0;
                        }else{
                                index = 1;
                                y += ty;
                        }
                }else{
                        if(pos.y < (y + ty)){
                                index = 3;
                        }else{
                                index = 2;
                                y += ty;
                        }
                        x += tx;
                }
                if(m_spaces[i] != index){
                        return i*4 + index;
                }
                i++;
        }
        return -1;
}
#endif /* D3 */
bool
WaypointTable::ImproveEntryPoint(Waypoint & waypoint){
	Ipv4Address empty;
//	return false;
//        std::cerr << "Improving entry point\n";
	NS_LOG_ERROR("Waypoint to improve: " << waypoint);
	if(waypoint.GetAddress() == empty){
		NS_LOG_ERROR(" but empty");
		return false;
	}
	if(waypoint.GetEntryPoint().GetHops() == 0){
		NS_LOG_ERROR(" but 0 hops");
		return false;
	}
	Neighbor hEntryPoint = waypoint.GetEntryPoint();
	Neighbor bestEntryPoint = hEntryPoint;
	int headerTraceSize = waypoint.GetTrace().size();

	int bestTraceSize = headerTraceSize;
	Waypoint bestWaypoint;
	for (std::map<int, std::vector<Waypoint> >::const_iterator mapIt = table.begin (); mapIt!= table.end (); ++mapIt){
		for(std::vector<Waypoint>::const_iterator vectorIt = mapIt->second.begin(); vectorIt != mapIt->second.end(); vectorIt++){
			Neighbor curEntryPoint = vectorIt->GetEntryPoint();
			Ipv4Address hAddr = hEntryPoint.GetAddress();
			Ipv4Address curAddr = curEntryPoint.GetAddress();
			NS_LOG_ERROR(vectorIt->GetAddress() << " entryPoint: " << curAddr << " (" << hAddr << ")");
			if((curAddr != empty) && (hAddr == curAddr)){
			  NS_LOG_ERROR("got waypoint with the same entry point " << curEntryPoint);
			   if(hEntryPoint.GetHops() >= curEntryPoint.GetHops()){
				NS_LOG_WARN("has better metric " << hEntryPoint.GetHops() << " vs " << curEntryPoint.GetHops());
				int currentTraceSize = 0;
				std::vector<Neighbor> trace = vectorIt->GetTrace();
				for (std::vector<Neighbor>::reverse_iterator j = trace.rbegin(); j!= trace.rend (); ++j){
					if(j->GetAddress() == curEntryPoint.GetAddress()) break;
					currentTraceSize++;
				}
				if(currentTraceSize > bestTraceSize){
					NS_LOG_WARN("has longer trace " << currentTraceSize << " vs " << bestTraceSize);
					bestTraceSize = currentTraceSize;
					bestWaypoint = *vectorIt;
				}else{
					NS_LOG_WARN("but shorter trace " << currentTraceSize << " vs " << bestTraceSize);
				}
			     }else{
				NS_LOG_WARN("but has worse metric " << hEntryPoint.GetHops() << " vs " << curEntryPoint.GetHops());
		      	     }
			}

		}
	}
	NS_LOG_WARN("bestTraceSize: " <<  bestTraceSize << " vs headerTraceSize: " << headerTraceSize);
	if(bestTraceSize > headerTraceSize){
		waypoint.SetTrace(bestWaypoint.GetTrace());
		NS_LOG_WARN("Improved waypoint " << waypoint);
		return true;
	}
	NS_LOG_WARN("Can't improve");
	return false;
}

bool WaypointTable::PurgeNode(Ipv4Address addr){
	//std::cout << "Purging " << addr << "\n Before purging: \n";
	//std::cout << *this;
	DeleteNeighbor (addr);

        for (std::map<int, std::vector<Waypoint> >::iterator mapIt = table.begin (); mapIt!= table.end (); ++mapIt){
		for(std::vector<Waypoint>::iterator vectorIt = mapIt->second.begin(); vectorIt != mapIt->second.end(); vectorIt++){
			int deleted = 0;
			if(addr == vectorIt->GetAddress()){
				mapIt->second.erase(vectorIt);
				deleted = 1;
				break;			
			}
			std::vector<Neighbor> trace = vectorIt->GetTrace();
			for(std::vector<Neighbor>::iterator traceIt = trace.begin(); traceIt != trace.end(); traceIt++){
				if(traceIt->GetAddress() == addr){
					//std::cout << "Doing it!\n";
					mapIt->second.erase(vectorIt);
					deleted = 1;
					break;			
				}
			}
			if(deleted) break;
			
		}
	}

	//std::cout << "After purging: \n";
	//std::cout << *this;
	return false;
}


uint32_t
WaypointTable::WaypointTableSize ()
{
  return table.size ();
}

void
WaypointTable::Print (std::ostream &os) const
{
  return;
  os << "\nWPR Neighbor table(" << m_pos << ")\n" << "Addr\t\tPosition\n";
  for (std::map<Ipv4Address, Neighbor>::const_iterator i = m_ipv4AddressEntry.begin (); i
       != m_ipv4AddressEntry.end (); ++i)
    {
      os << i->second;
    }

  os << "Diameter: " << m_diameter << " Spaces:";
  for( int i=0; i < MAX_SPACES_DEPTH * SPACES_DIVISION; i++)
	os << m_spaces[i];
  os << "\n";

  os << "\nWPR Waypoint table\n" << "Space\t\tAddr\t\tPosition\n";
  for (std::map<int, std::vector<Waypoint> >::const_iterator i = table.begin (); i
       != table.end (); ++i){
	for(std::vector<Waypoint>::const_iterator j = i->second.begin(); j != i->second.end(); j++){
  	      os << i->first << "\t" << *j;
	}
    }
  os << "\n";
}


void
Waypoint::Print (Ptr<OutputStreamWrapper> stream) const
{
  *stream->GetStream () << std::setiosflags (std::ios::fixed) << "Addr:" << m_addr << "\t\t" << "pos: " << m_pos << "hops: " << m_hops << "dist: " << m_dist << "\n";

}

void
Waypoint::Print (std::ostream &os) const
{
  os <<  "Addr:" << m_addr << "\t\t" << "pos:" << m_pos << " hops:" << m_hops << " dist: " << m_dist << " timestamp:  " << m_timestamp << " trace:\n";
  for(unsigned int j = 0; j < m_trace.size(); j++)
  {
	os << m_trace[j];
  }
  os << "EOT\n";
  os << "Entry point:" << m_entryPoint << " hops: " << m_entryPoint.GetHops() << " dist: " << m_entryPoint.GetDist();

}


}
}
