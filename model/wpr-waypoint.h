#ifndef WPR_WAYPOINT_H
#define WPR_WAYPOINT_H

#include "wpr-ntable.h"
#include <cassert>
#include <map>
#include <sys/types.h>
#include <iostream>
#include "ns3/ipv4.h"
#include "ns3/ipv4-route.h"
#include "ns3/timer.h"
#include "ns3/net-device.h"
#include "ns3/output-stream-wrapper.h"
#include "ns3/vector.h"
#include <vector>
#include "ns3/packet.h"
#include "wpr-common.h"

namespace ns3 {
namespace wpr {


class Waypoint: public Neighbor
{
public:
  Waypoint (Vector pos = Vector(),
                     Ipv4Address  addr = Ipv4Address (), int hops = 0, int timestamp = 0, double dist = 0);
  ~Waypoint ();

Neighbor GetEntryPoint () const
{
    return m_entryPoint;
}

void
SetEntryPoint (Neighbor entryPoint)
{
    m_entryPoint = entryPoint;
}

std::vector<Neighbor> GetTrace () const  
{
    return m_trace;
}
/*
void ShiftTrace();

*/
bool
  PopTrace(Ipv4Address & addr);

bool
  GetNextHop(Ipv4Address & addr);

void
 UpdateTrace(Neighbor n);

void
 SetTrace(std::vector<Neighbor> trace);

void
  Print (Ptr<OutputStreamWrapper> stream) const;
void Print (std::ostream &os) const;

bool
  IsBetterWaypointThan(Neighbor target, Waypoint w);

int64_t 
  GetTimestamp(){
	return m_timestamp;
  }
void 
  SetTimestamp(int64_t time){
	m_timestamp = time;
}
/* Moved to neighbor
double 
  GetDist() const {
	return m_dist;
  }
void 
  SetDist(int64_t dist){
	m_dist = dist;
}
*/
double 
  CalculateMetric(Vector pos);

private:
  std::vector<Neighbor> m_trace;
  Neighbor m_entryPoint;
  int64_t m_timestamp;
//  double m_dist;
};

static inline std::ostream & operator<< (std::ostream& os, const Waypoint & waypoint)
{
  waypoint.Print (os);
  return os;
}


class WaypointTable : public NeighborTable
{
public:
  WaypointTable ();
  ~WaypointTable();
  bool
  AddWaypoint (Waypoint & n, Vector pos);
  bool
  DeleteWaypoint (int index);
  bool
  LookupWaypoint (int index, std::vector<Waypoint> & n);
  bool
  LookupClosestWaypoint (Vector pos, Waypoint waypoint, Waypoint & target, Packet packet);
  bool
  LookupNodeInTrace (Ipv4Address addr, Waypoint & target);
  bool
  ImproveEntryPoint(Waypoint & waypoint);
  uint32_t
  WaypointTableSize ();

  int GetWaypointIndex(Vector pos);  
  void
  Print (std::ostream &os) const;
  bool PurgeNode(Ipv4Address addr);
  /*bool
  CachePacket(Packet packet, Ipv4Address addr);
  bool
  PacketInCache(Packet packet, Ipv4Address addr);*/
private:
  std::map<int, std::vector<Waypoint> > table;
};

static inline std::ostream & operator<< (std::ostream& os, const WaypointTable & table)
{
  table.Print (os);
  return os;
}
}
}
#endif /* WPR_WAYPOINT_H */

