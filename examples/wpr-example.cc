/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2010 Hemanth Narra
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Hemanth Narra <hemanth@ittc.ku.com>
 *
 * James P.G. Sterbenz <jpgs@ittc.ku.edu>, director
 * ResiliNets Research Group  http://wiki.ittc.ku.edu/resilinets
 * Information and Telecommunication Technology Center (ITTC)
 * and Department of Electrical Engineering and Computer Science
 * The University of Kansas Lawrence, KS USA.
 *
 * Work supported in part by NSF FIND (Future Internet Design) Program
 * under grant CNS-0626918 (Postmodern Internet Architecture),
 * NSF grant CNS-1050226 (Multilayer Network Resilience Analysis and Experimentation on GENI),
 * US Department of Defense (DoD), and ITTC at The University of Kansas.
 */
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/mobility-module.h"
#include "ns3/config-store-module.h"
#include "ns3/wifi-module.h"
#include "ns3/internet-module.h"
#include "ns3/wpr-helper.h"
#include "ns3/wpr-common.h"
#include <iostream>
#include <cmath>

 
using namespace ns3;

uint16_t port = 9;

std::vector<Vector2D> lost;
std::vector<ns3::Timer> timers;
unsigned int global_transmissions;
unsigned int transmissions_counter = 0;
unsigned int sent_counter = 0;

NS_LOG_COMPONENT_DEFINE ("WprManetExample");

class WprManetExample
{
public:
  WprManetExample ();
  void CaseRun (bool withReversePacket, bool withWaypoint, bool withEntryPoint, int nodes, int mode, const std::string topoFile, double maxX, double maxY, double maxZ, double totalTime,
                std::string rate,
                std::string phyMode,
                uint32_t periodicUpdateInterval,
                double dataStart,
                bool printRoutes,
		double lossRate, bool symetric, bool retransmit);

private:
  uint32_t m_nodes;
  std::string m_topoFile;
  double m_totalTime;
  std::string m_rate;
  std::string m_phyMode;
  uint32_t m_periodicUpdateInterval;
  double m_dataStart;
  bool m_printRoutes;
  double m_maxX;
  double m_maxY;
  double m_maxZ;
  double m_lossRate;
  unsigned int sent;
  unsigned int received;
  bool m_flag;
  bool m_withEntryPoint;
  bool m_withWaypoint;
  bool m_withReversePacket;
  bool m_symetric;
  bool m_retransmit;
  int m_source;
  int m_destination;
  
  NodeContainer nodes;
  NetDeviceContainer devices;
  Ipv4InterfaceContainer interfaces;

private:
  void CreateNodes ();
  void CreateDevices ();
  void InstallInternetStack ();
  void InstallApplications ();
  void SetupMobilityFromFile ();
  void SetupMobilityRandom ();
  void ReceivePacket (Ptr <Socket> );
  void SetupPacketReceive ( );
  void CheckThroughput ();
  void InstallSinks();
  void PrintStats();
  void SendPacket(int source, int destination);
  void ResendPackets();

};

int main (int argc, char **argv)
{
  WprManetExample test;
  std::string topoFile = "first.tp";
  int transmissions = 1;
  std::string rate ("8kbps");
  std::string phyMode ("DsssRate11Mbps");
  std::string appl = "all";
  uint32_t periodicUpdateInterval = 6;
  double dataStart = 100.0;
  bool printRoutingTable = true;
  int mode = 1;
  double maxX = 400;
  double maxY = 400;
  double maxZ = 400;
  double lossRate = 0;
  uint32_t  nodes = 150;
  bool withEntryPoint = true;
  bool withWaypoint = true;
  bool withReversePacket = true;
  bool withExplore = true;
  bool symetric = false;
  bool retransmit = false;

  LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_INFO);
  LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_INFO);
  //LogComponentEnable ("WprPacket", LOG_LEVEL_DEBUG);


  CommandLine cmd;
  cmd.AddValue ("topoFile", "File with a topology[Default: first.tp", topoFile);
  cmd.AddValue ("maxX", "X dimension[Default: 400]", maxX);
  cmd.AddValue ("withWaypoint", "Do we use waypoints", withWaypoint);
  cmd.AddValue ("withEntryPoint", "Do we use entry points", withEntryPoint);
  cmd.AddValue ("withReversePacket", "Do we use reverse packet", withReversePacket);
  cmd.AddValue ("withExplore", "Do we use exploring", withExplore);
  cmd.AddValue ("maxY", "Y dimension[Default: 400]", maxY);
  cmd.AddValue ("maxZ", "Z dimension[Default: 400]", maxZ);
  cmd.AddValue ("lossRate", "Number of nodes offline [Default: 0]", lossRate);
  cmd.AddValue ("nodes", "Number of nodes[Defaults: 150]", nodes);
  cmd.AddValue ("mode", "Mode [Default: 1(random topology)], (2 read from file)", mode);
  cmd.AddValue ("transmissions", "Number of transmissions[Default:100]", transmissions);
  cmd.AddValue ("periodicUpdateInterval", "Periodic Interval Time[Default=15]", periodicUpdateInterval);
  cmd.AddValue ("dataStart", "Time at which nodes start to transmit data[Default=50.0]", dataStart);
  cmd.AddValue ("printRoutingTable", "print routing table for nodes[Default:1]", printRoutingTable);
  cmd.AddValue ("symetric", "symetric traffic [Default: 0]", symetric);
  cmd.AddValue ("retransmit", "retransmission [Default: 0]", retransmit);
  cmd.Parse (argc, argv);

  std::cout << "Nodes = " << nodes << "\n";

  //SeedManager::SetSeed (time(NULL));
  SeedManager::SetSeed (1234);

  Config::SetDefault ("ns3::OnOffApplication::PacketSize", StringValue ("1000"));
  Config::SetDefault ("ns3::OnOffApplication::DataRate", StringValue (rate));
  Config::SetDefault ("ns3::WifiRemoteStationManager::NonUnicastMode", StringValue (phyMode));
  Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", StringValue ("2000"));




  test = WprManetExample ();
  double totalTime = dataStart + (transmissions * 5);
  global_transmissions = transmissions;
  transmissions_counter = 0;
  test.CaseRun (withReversePacket, withWaypoint, withEntryPoint, nodes, mode, topoFile, maxX, maxY, maxZ,  totalTime, rate, phyMode, periodicUpdateInterval,
                dataStart, printRoutingTable, lossRate, symetric, retransmit);

  return 0;
}

WprManetExample::WprManetExample ()
  : sent(0),
    received(0)
{
}

void
WprManetExample::ReceivePacket (Ptr <Socket> socket)
{
  NS_LOG_UNCOND (Simulator::Now ().GetSeconds () << " Received one packet!");
  Ptr <Packet> packet;
  while ((packet = socket->Recv ()))
    {
      received++;
    }
	std::vector<Vector2D>::iterator it;
	for(it = lost.begin(); it != lost.end(); ){
		//NS_LOG_UNCOND( "checking " << *it << " against " << m_source << "->" << m_destination << "\n");
		if((it->x == m_source) && (it->y == m_destination)){
			lost.erase(it);
		}else{
			it++;
		}
			
	}
  if(m_symetric && (m_source != -1) && (m_destination != -1)){ 
	std::cout << "Sending response!!!!\n";
        sent++;
        UdpEchoClientHelper echoClient (interfaces.GetAddress (m_source), 9);
        echoClient.SetAttribute ("MaxPackets", UintegerValue (1));
        echoClient.SetAttribute ("Interval", TimeValue (Seconds (5.0)));
        echoClient.SetAttribute ("PacketSize", UintegerValue (1024));

        ApplicationContainer clientApps = echoClient.Install (nodes.Get (m_destination));
        Ptr<UniformRandomVariable> var = CreateObject<UniformRandomVariable> ();
        clientApps.Start (Seconds (0));
        clientApps.Stop (Seconds (5));



	m_source = -1;
	m_destination = -1;
  }
}

void
WprManetExample::SetupPacketReceive ()
{

  for(unsigned int i = 0; i < m_nodes; i++){
	  Ptr<Node> node = NodeList::GetNode (i);
	  Ipv4Address addr = node->GetObject<Ipv4> ()->GetAddress (1, 0).GetLocal ();
	
	  TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
	  Ptr <Socket> sink = Socket::CreateSocket (node, tid);
	  InetSocketAddress local = InetSocketAddress (addr, 9);
	  sink->Bind (local);
	  sink->SetRecvCallback (MakeCallback ( &WprManetExample::ReceivePacket, this));
  }

}

void
WprManetExample::CaseRun (bool withReversePacket, bool withWaypoint, bool withEntryPoint, int nodes, int mode, const std::string topoFile, double maxX, double maxY, double maxZ, double totalTime, std::string rate,
                           std::string phyMode, uint32_t periodicUpdateInterval,
                           double dataStart, bool printRoutes, double lossRate, bool symetric, bool retransmit)
{
  m_topoFile = topoFile;
  m_nodes = nodes;
  m_totalTime = totalTime;
  m_rate = rate;
  m_phyMode = phyMode;
  m_periodicUpdateInterval = periodicUpdateInterval;
  m_dataStart = dataStart;
  m_printRoutes = printRoutes;
  m_maxX = maxX;
  m_maxY = maxY;
  m_maxZ = maxZ;
  m_withWaypoint = withWaypoint;
  m_withEntryPoint = withEntryPoint;
  m_withReversePacket = withReversePacket;
  m_lossRate = lossRate;
  m_symetric = symetric;
  m_retransmit = retransmit;
  m_source = -1;
  if(mode == 2){
	  m_nodes = 0;
	  std::ifstream in(m_topoFile.c_str());
	  while (!in.eof()){
		Vector v;
		in >> v;
		if(in.peek() == EOF) break;
		if(m_maxX < v.x) m_maxX = v.x;
		if(m_maxY < v.y) m_maxY = v.y;
		if(m_maxZ < v.z) m_maxY = v.z;
		m_nodes++;
		//std::cout << m_nodes  << "[pos=\""<< v.x << ", " << v.y<< "!\", " << "label=" << m_nodes << ", width=10.5, style=filled, shape=circle" << "\n"; 
  	}
  }


  CreateNodes ();
  CreateDevices ();
  if(mode == 1){
	SetupMobilityRandom ();
  }else if(mode ==2){
	SetupMobilityFromFile();
  }else{
	std::cerr << "Unknown mode!\n";
	return;
  }
  InstallInternetStack ();
  SetupPacketReceive();

  
  Simulator::Schedule(Seconds(m_dataStart),  &WprManetExample::InstallApplications, this);

  Simulator::Stop (Seconds (m_totalTime) + Seconds(100000));
  Simulator::Run ();
  PrintStats();
  Simulator::Destroy ();
}

void 
WprManetExample::PrintStats(){
  std::cout << "Received " << received << "/" << sent << " packets\n";
  std::cout << lost.size() << " packets in retrnasmit vector\n";
/*  std::vector<Vector2D>::iterator it;
  for(it = lost.begin(); it != lost.end(); it++ ){
	std::cout << it->x << "->" << it->y << "\n";
  }*/
  

  
}

void
WprManetExample::CreateNodes ()
{
  std::cout << "Creating " << (unsigned) m_nodes << " nodes.\n";
  nodes.Create (m_nodes);
}

void
WprManetExample::SetupMobilityRandom ()
{
  MobilityHelper mobility;
  ObjectFactory pos;
#if D3
  pos.SetTypeId ("ns3::RandomBoxPositionAllocator");
#else
  pos.SetTypeId ("ns3::RandomRectanglePositionAllocator");
#endif 
  char buf[100];
  sprintf(buf, "ns3::UniformRandomVariable[Min=0.0|Max=%f]", m_maxX);
  pos.Set ("X", StringValue (buf));
  sprintf(buf, "ns3::UniformRandomVariable[Min=0.0|Max=%f]", m_maxY);
  pos.Set ("Y", StringValue (buf));
#if D3
  sprintf(buf, "ns3::UniformRandomVariable[Min=0.0|Max=%f]", m_maxZ);
  pos.Set ("Z", StringValue (buf));
#endif

  Ptr <PositionAllocator> taPositionAlloc = pos.Create ()->GetObject <PositionAllocator> ();
  mobility.SetPositionAllocator (taPositionAlloc);
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (nodes);

  int i = 1;
  uint32_t nodes, itr;
  nodes = NodeList::GetNNodes();

  std::ofstream off("topo.dat");

  for(itr = 0; itr<nodes; itr++)
    {
	Ptr<MobilityModel> loc = (NodeList::GetNode(itr))->GetObject<MobilityModel> ();
	Vector v = loc->GetPosition ();	
        std::cerr << i  << "[pos=\""<< v.x << ", " << v.y<< "!\", " << "label=" << i << ", width=10.5, style=filled, shape=circle" << "\n";
        off << v.x << ":"<< v.y << ":" << v.z << "\n";
 	i++;
  }
  off.close();

}

void
WprManetExample::SetupMobilityFromFile ()
{

  MobilityHelper mobility;
  Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
  
  std::ifstream in(m_topoFile.c_str());
  while (!in.eof()){
	Vector v;
	in >> v;
	if(in.peek() == EOF) break;
	std::cerr << v << '\n'; 
	positionAlloc->Add(v);
  }
  in.close();
  mobility.SetPositionAllocator (positionAlloc);
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (nodes);
}

void
WprManetExample::CreateDevices ()
{
  NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default ();
  wifiMac.SetType ("ns3::AdhocWifiMac");
  YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();
  YansWifiChannelHelper wifiChannel;
  wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
  wifiChannel.AddPropagationLoss ("ns3::RangePropagationLossModel");
  wifiPhy.SetChannel (wifiChannel.Create ());
  WifiHelper wifi;
  wifi.SetStandard (WIFI_PHY_STANDARD_80211g);
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager", "DataMode", StringValue (m_phyMode), "ControlMode",
                                StringValue (m_phyMode));
  devices = wifi.Install (wifiPhy, wifiMac, nodes);

}

void
WprManetExample::InstallInternetStack ()
{
  WprHelper wpr;
  wpr.Set ("PeriodicUpdateInterval", TimeValue (Seconds (m_periodicUpdateInterval)));
  wpr.Set ("MaxX", DoubleValue(m_maxX + 10.0));
  wpr.Set ("MaxY", DoubleValue(m_maxY + 10.0));
#if D3
  wpr.Set ("MaxZ", DoubleValue(m_maxZ + 10.0));
#endif
  wpr.Set ("withWaypoint", BooleanValue(m_withWaypoint));
  wpr.Set ("withEntryPoint", BooleanValue(m_withEntryPoint));
  wpr.Set ("withReversePacket", BooleanValue(m_withReversePacket));
  wpr.Set ("lossRate", DoubleValue(m_lossRate));
 // wpr.Set ("maxWaypoints", UintegerValue(m_maxWaypoints));
 // wpr.Set ("maxTrace", UintegerValue(m_maxTrace));
  //wpr.Set("withBalancedWaypoints", BooleanValue(m_withBalancedWaypoints));
  InternetStackHelper stack;
  stack.SetRoutingHelper (wpr); // has effect on the next Install ()
  stack.Install (nodes);
  Ipv4AddressHelper address;
  address.SetBase ("10.1.0.0", "255.255.0.0");
  interfaces = address.Assign (devices);
  if (m_printRoutes)
    {
      Ptr<OutputStreamWrapper> routingStream = Create<OutputStreamWrapper> (("wpr.routes"), std::ios::out);
      wpr.PrintRoutingTableAllAt (Seconds (m_totalTime), routingStream);
    }

  std::ofstream out("map.dat");
  ns3::NodeContainer::Iterator it;
  for(it = nodes.Begin(); it != nodes.End(); it++){
	out << (*it)->GetId() << "#" << (*it)->GetObject<Ipv4>()->GetAddress (1, 0).GetLocal () << "\n";
  }
  out.close();
}


void
WprManetExample::InstallSinks ()
{
	UdpEchoServerHelper echoServer (9);
	ApplicationContainer serverApps = echoServer.Install (nodes);
	serverApps.Start (Seconds (0));
	serverApps.Stop (Seconds (m_totalTime));

}
void
WprManetExample::InstallApplications ()
{

	if(transmissions_counter++ == global_transmissions) return;
        int nNodes = (int) nodes.GetN ();
	m_destination = m_source = rand() % nNodes;
	while (m_source == m_destination){
		m_source = rand() % nNodes;
	}
  	//if(!m_flag){
	//        receiver = 0;
	//	sender = 10;	
	/*}else{
	        receiver = 11;
		sender = 0;
	}
	m_flag = !m_flag;	*/
//	m_source = 0;
//	m_destination = 10;
	sent++;
	sent_counter++;
	SendPacket(m_source, m_destination);

	Vector2D v;
	v.x = m_source;
	v.y = m_destination;
	lost.push_back(v);
//	std::cerr << "put " << v << "\n";
	
	if(!(sent_counter % 4) && m_retransmit){
		Simulator::Schedule(Seconds(5),  &WprManetExample::ResendPackets, this);
	}else{
		Simulator::Schedule(Seconds(5),  &WprManetExample::InstallApplications, this);
	}
}

void
WprManetExample::SendPacket(int source, int destination){
	NS_LOG_UNCOND( "sending: " << source << "->" << destination << "\n");
	m_source = source;
	m_destination = destination;
	UdpEchoClientHelper echoClient (interfaces.GetAddress (m_destination), 9);
	echoClient.SetAttribute ("MaxPackets", UintegerValue (1));
	echoClient.SetAttribute ("Interval", TimeValue (Seconds (5.0)));
	echoClient.SetAttribute ("PacketSize", UintegerValue (1024));
		
	ApplicationContainer clientApps = echoClient.Install (nodes.Get (m_source));
	Ptr<UniformRandomVariable> var = CreateObject<UniformRandomVariable> ();
	clientApps.Start (Seconds (0));
	clientApps.Stop (Seconds (5));
}

void
WprManetExample::ResendPackets ()
{
	NS_LOG_UNCOND("################## RESENDING ################### \n");
	std::vector<Vector2D>::iterator it;
	unsigned int counter = 1;	
	for(it = lost.begin(); it != lost.end(); it++){
		NS_LOG_UNCOND( "retransmitting: " << *it << "\n");
/*		Timer timer (Timer::CHECK_ON_DESTROY );
		timer.SetFunction (&WprManetExample::SendPacket, this);
		timer.SetArguments ((int)it->x, (int)it->y);
		timer.Schedule (Seconds (counter++ * 5));*/
		Simulator::Schedule(Seconds(counter++ * 5), &WprManetExample::SendPacket, this, it->x, it->y);
	}
	Simulator::Schedule(Seconds(counter * 5),  &WprManetExample::InstallApplications, this);
}
