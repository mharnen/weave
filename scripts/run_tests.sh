#!/bin/bash
export NS_LOG="WprRoutingProtocol=level_debug|prefix_node"
./waf --run "wpr-example -mode=1 -transmissions=100  -symetric=0 -maxX=800 -maxY=800 -nodes=600 -withWaypoint=0 -withEntryPoint=0 -withReversePacket=0"  &> log000_600 
PID1=$!
./waf --run "wpr-example -mode=1 -transmissions=100  -symetric=0 -maxX=800 -maxY=800 -nodes=600 -withWaypoint=1 -withEntryPoint=0 -withReversePacket=0"  &> log100_600 &
PID2=$!
./waf --run "wpr-example -mode=1 -transmissions=100  -symetric=0 -maxX=800 -maxY=800 -nodes=600 -withWaypoint=1 -withEntryPoint=1 -withReversePacket=0"  &> log110_600 &
PID3=$!
./waf --run "wpr-example -mode=1 -transmissions=100  -symetric=0 -maxX=800 -maxY=800 -nodes=600 -withWaypoint=1 -withEntryPoint=1 -withReversePacket=1"  &> log111_600 &
PID4=$!
wait $PID1 $PID2 $PID3 $PID4 
./calculate_stretch.sh log000_600 &> cs_log000_600
./calculate_stretch.sh log100_600 &> cs_log100_600
./calculate_stretch.sh log110_600 &> cs_log110_600
./calculate_stretch.sh log111_600 &> cs_log111_600
exit
rm log*

./waf --run "wpr-example -mode=1 -transmissions=100  -symetric=0 -maxX=800 -maxY=800 -nodes=700 -withWaypoint=0 -withEntryPoint=0 -withReversePacket=0"  &> log000_650   &
PID1=$!
./waf --run "wpr-example -mode=1 -transmissions=10000  -symetric=0 -maxX=800 -maxY=800 -nodes=700 -withWaypoint=1 -withEntryPoint=0 -withReversePacket=0"  &> log100_650 &
PID2=$!
./waf --run "wpr-example -mode=1 -transmissions=10000  -symetric=0 -maxX=800 -maxY=800 -nodes=700 -withWaypoint=1 -withEntryPoint=1 -withReversePacket=0"  &> log110_650 & 
PID3=$!
./waf --run "wpr-example -mode=1 -transmissions=10000  -symetric=0 -maxX=800 -maxY=800 -nodes=700 -withWaypoint=1 -withEntryPoint=1 -withReversePacket=1"  &> log111_650 &
PID4=$!
wait $PID1 $PID2 $PID3 $PID4 
./calculate_stretch.sh log000_650 &> cs_log000_650
./calculate_stretch.sh log100_650 &> cs_log100_650
./calculate_stretch.sh log110_650 &> cs_log110_700
./calculate_stretch.sh log111_650 &> cs_log111_650
rm log*


./waf --run "wpr-example -mode=1 -transmissions=100  -symetric=0 -maxX=800 -maxY=800 -nodes=700 -withWaypoint=0 -withEntryPoint=0 -withReversePacket=0"  &> log000_700 &  
PID1=$!
./waf --run "wpr-example -mode=1 -transmissions=10000  -symetric=0 -maxX=800 -maxY=800 -nodes=700 -withWaypoint=1 -withEntryPoint=0 -withReversePacket=0"  &> log100_700 &
PID2=$!
./waf --run "wpr-example -mode=1 -transmissions=10000  -symetric=0 -maxX=800 -maxY=800 -nodes=700 -withWaypoint=1 -withEntryPoint=1 -withReversePacket=0"  &> log110_700 & 
PID3=$!
./waf --run "wpr-example -mode=1 -transmissions=10000  -symetric=0 -maxX=800 -maxY=800 -nodes=700 -withWaypoint=1 -withEntryPoint=1 -withReversePacket=1"  &> log111_700 & 
PID4=$! 
wait $PID1 $PID2 $PID3 $PID4 
./calculate_stretch.sh log000_700 &> cs_log000_700
./calculate_stretch.sh log100_700 &> cs_log100_700
./calculate_stretch.sh log110_700 &> cs_log110_700
./calculate_stretch.sh log111_700 &> cs_log111_700
rm log*
