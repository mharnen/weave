#!/bin/bash

LOGFILE=log.dat
MAPFILE=map.dat
TOPOFILE=topo.dat
TRACEFILE=trace.dat 
DIJKSTRAFILE=dijkstra.dat
STEP=100
echo -n > $TRACEFILE

grep [#\?\!] $1 &> log.dat

PACKETS=( $(grep "^[0-9]\{1,4\} \!" $LOGFILE | sed 's/ /#/g') )
for i in "${PACKETS[@]}"
do
   PACKET_ID=`echo $i | cut -d '!' -f 2 | cut -d '#' -f 1`
#   STRETCH=`grep "?$PACKET_ID" $LOGFILE | wc -l`
   SRC=`echo $i | cut -d '#' -f 3 | cut -d '(' -f 1`
   DST=`echo $i | cut -d '>' -f 3 `
   SRC_ID=`grep "$SRC$" $MAPFILE | cut -d '#' -f 1`
   DST_ID=`grep "$DST$" $MAPFILE | cut -d '#' -f 1`
   IS_RECEIVED=`grep "#$PACKET_ID" $LOGFILE  | wc -l`
   echo "$SRC_ID->$DST_ID" >> $TRACEFILE
done
java dijkstra.Dijkstra $TOPOFILE $TRACEFILE > $DIJKSTRAFILE

ALL=0
LINE=1
ALL_STEP=0
RECEIVED=0
RECEIVED_STEP=0
DIJKSTRA_SUM=0
DIJKSTRA_STEP=0
STRETCH_SUM=0
STRETCH_STEP=0
COUNTER=0
for i in "${PACKETS[@]}"
do
   let LINE=$LINE+1
   let COUNTER=$COUNTER+1
   PACKET_ID=`echo $i | cut -d '!' -f 2 | cut -d '#' -f 1`
   STRETCH=`grep "?$PACKET_ID" $LOGFILE | wc -l`
   SRC=`echo $i | cut -d '#' -f 3 | cut -d '(' -f 1`
   DST=`echo $i | cut -d '>' -f 3 `
   SRC_ID=`grep "$SRC$" $MAPFILE | cut -d '#' -f 1`
   DST_ID=`grep "$DST$" $MAPFILE | cut -d '#' -f 1`
   IS_RECEIVED=`grep "#$PACKET_ID " $LOGFILE  | wc -l`
   DIJKSTRA=`sed -n "${LINE}p" < $DIJKSTRAFILE | cut -d '=' -f 2`
#   echo "got dijkstra: `sed -n "${LINE}p" < $DIJKSTRAFILE`"
   let STRETCH=$STRETCH+1
   if [ $IS_RECEIVED -eq 1 ]
   then
	let ALL_STEP=$ALL_STEP+1
   	let ALL=$ALL+1
	let RECEIVED=$RECEIVED+1
	let RECEIVED_STEP=$RECEIVED_STEP+1
        if (( $STRETCH < $DIJKSTRA ))
	then
		echo "##########Dijkstra worse than wpr############"
	fi
   	let DIJKSTRA_SUM=$DIJKSTRA_SUM+$DIJKSTRA
	let STRETCH_SUM=$STRETCH_SUM+$STRETCH
   	let DIJKSTRA_STEP=$DIJKSTRA_STEP+$DIJKSTRA
	let STRETCH_STEP=$STRETCH_STEP+$STRETCH
   fi
   if [ $IS_RECEIVED -eq 0 ] 
   then
	if [ ! $DIJKSTRA -eq 2147483647 ]
	then
#		echo "$PACKET_ID $SRC_ID->$DST_ID $STRETCH $DIJKSTRA $IS_RECEIVED"
		let ALL_STEP=$ALL_STEP+1
	   	let ALL=$ALL+1
	fi
   fi
   if [ $COUNTER -eq  $STEP ]
   then
#	echo "Partial packets: $RECEIVED_STEP/$ALL_STEP, stretch $DIJKSTRA_STEP/$STRETCH_STEP"
	COUNTER=0
	ALL_STEP=0
	RECEIVED_STEP=0
	DIJKSTRA_STEP=0
	STRETCH_STEP=0
   fi
done
echo Packets: $RECEIVED/$ALL
echo Stretch: $DIJKSTRA_SUM/$STRETCH_SUM
